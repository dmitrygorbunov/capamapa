# install
install nodejs
install mongodb (& creat C:\Data\db folder)
npm install
npm install -g nodemon

# Run
1) mongod (run mongoDB from C:\Program Files\MongoDB\Server\4.0\bin)
2) Run node server http://localhost:3000
nodemon ./bin/www  
npm run nodemon ./bin/www 
3) # Run angular app  http://localhost:4200
ng serve
npm run ng serve
ng serve --proxy-config proxy.config.json  
npm serve --proxy-config proxy.config.json

# Run mock server
Run `json-server --watch db.json`

# heroku 
1) git push heroku master (push to heroku)
2) git remote rm heroku (remove geroku remote)
heroku git:remote -a capoeira-map (change branch?)
heroku git:remote -a test-capoeira-map (change branch #2)
3) git remote -v (check remote url)

# get events from facebook 
http://localhost:3000/events/getFacebookEvents
http://test-capoeira-map.herokuapp.com/events/getFacebookEvents
https://www.capoeira-map.com/events/getFacebookEvents

# mlab.com
Login: heroku_z2zxlmpp