import { Component } from '@angular/core';
import { LoginService } from './services/login/login.service';
import { UsersService } from './services/users/users.service';
//import { GroupsService } from './services/groups/groups.service';
import { CountriesService } from './services/countries/countries.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent {
   closeMainSidebar: boolean; 
   openDetailsSidebar: boolean;
   showMobileMap: boolean;
   showFilters: boolean;
   openAddEventSidebar: boolean;
   clearSelectedEvent: boolean;
   selectedEvent;
   title: string = 'My first angular2-google-maps project';
   globalLat: number = 49.9927326;
   globalLng: number = 36.23085022;
   displayLeftSidebar = false;
   msgs = "";
   editableEvent = false;

  constructor(
    private loginService: LoginService,
    private usersService: UsersService,
    //private groupsService: GroupsService,
    private countriesService: CountriesService,
    translate: TranslateService
  ) {
   this.closeMainSidebar = false;
   this.showMobileMap = false;
   this.showFilters = false;
   this.openDetailsSidebar = false;
   this.openAddEventSidebar = false;
   this.selectedEvent = {}
   this.clearSelectedEvent = false;
   translate.addLangs(['en', 'br'])
   translate.setDefaultLang('en');
   translate.use('en');
  }

  ngOnInit() {
    this.usersService.getUsers();
    //this.groupsService.getGroups();
    this.countriesService.getCountries();
    this.loginService.checkUserExpireDate();
  }  

  closeSidebar() {
    this.openAddEventSidebar = false;
    this.openDetailsSidebar = false;
    this.clearSelectedEvent = true;
    this.selectedEvent = {};
  }

  selectEvent(selectedEvent) {
    if(this.selectedEvent == selectedEvent){
      this.openDetailsSidebar = false;
      this.openAddEventSidebar = false;
      this.clearSelectedEvent = false;
      this.selectedEvent = {}; 
      
    } else {
        this.selectedEvent = selectedEvent;
        this.openDetailsSidebar = true;
        this.openAddEventSidebar = false;
        this.clearSelectedEvent = false;
        this.showMobileMap = false;
    }
  }

  openAddEventSidebarFunc(){
    this.selectedEvent = {};
    this.openAddEventSidebar = true;
    this.openDetailsSidebar = false;
    this.clearSelectedEvent = true;
  }

  openEditEventSidebarFunc(){
    this.openAddEventSidebar = true;
    this.openDetailsSidebar = false;
    this.clearSelectedEvent = false;
  }

  isEditableEvent(isEditableEvent){
    this.editableEvent = isEditableEvent;
  }

  cancelEditing(){
    this.openDetailsSidebar = true;
    this.openAddEventSidebar = false;
    this.clearSelectedEvent = false;
  }

  showMobileMapFunc(){
    this.showMobileMap = true;
    this.showFilters = false;
  }

  showEventsListFunc(){
    this.showMobileMap = false;
    this.showFilters = false;
  }

  displayLeftSidebarFunc(){
    this.displayLeftSidebar = true;
  }

  showFiltersFunc(){
    this.showFilters = true;
  }


}
