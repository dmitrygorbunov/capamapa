import { EventEmitter, Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class GroupsService {
    PORT =  environment.production ? '' : 'http://localhost:3000' ;
    allGroups;
    constructor(
        private http: Http
    ){}

    getGroups()  {
        let that = this;
        return this.http.get(this.PORT + '/groups' )
            .toPromise()
            .then(res => res.json())
            .then(function(res){
                that.allGroups = res;         
            })
            .catch(this.handleError);
    }

    addGroup() {
        let that = this;
        return this.http.get(this.PORT + '/groups/add' )
        .toPromise()
        .then(res => res.json())
        .then(function(res){
            that.allGroups = res;         
        })
        .catch(this.handleError);
    }
    
    getGroupDetails(groupId){
        for(let i = 0; i < this.allGroups.length; i++) {
            if(this.allGroups[i]._id == groupId){
                return this.allGroups[i].groupName;
            }
        }
        return groupId;
    }

    /* Using for filtering drop-down list*/
    filterGroups(query, allGroups = this.allGroups) {
        let filtered : any[] = [];
        for(let i = 0; i < allGroups.length; i++) {
            let group = allGroups[i];
            if(group.groupName.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(group);
            }
        }
        return filtered;
    }

    private handleError(error: any): Promise<any> {
         console.error('Error', error);
         return Promise.reject(error.message || error);
    }    

}