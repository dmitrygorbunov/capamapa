import { EventEmitter, Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { UsersService } from '../../services/users/users.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class EventsService {
    selectedEvent;
    PORT =  environment.production ? '' : 'http://localhost:3000';
    public visibleEventsChanged$;
    public eventClicked$;
    public markerClicked$;
    public eventCreated$; 
    public eventDeleted$;  
    public eventDetailsLoading$; 
    public openMultyEventsPopup$; 
    public openDeletePopup$;
    public editEvent$; 
    public createEvent$; 
    public startFiltering$;
    public openConfirmConvertPopup$;
    public convertToMyEventConfirmed$;
    public searchByEvents$;
    public searchByGuestEvents$;

    constructor(
        private http: Http,
        private usersService: UsersService,
        private messageService: MessageService
    ){
        this.visibleEventsChanged$ = new EventEmitter();
        this.eventClicked$ = new EventEmitter();
        this.markerClicked$ = new EventEmitter();
        this.eventCreated$ = new EventEmitter();
        this.eventDeleted$ = new EventEmitter();
        this.eventDetailsLoading$ = new EventEmitter();
        this.openMultyEventsPopup$ = new EventEmitter();
        this.openDeletePopup$ = new EventEmitter();
        this.editEvent$ = new EventEmitter();
        this.createEvent$ = new EventEmitter();
        this.startFiltering$ = new EventEmitter();
        this.openConfirmConvertPopup$ = new EventEmitter();
        this.convertToMyEventConfirmed$ = new EventEmitter();
        this.searchByEvents$ = new EventEmitter();
        this.searchByGuestEvents$ = new EventEmitter();
    }

    getEvents(type = 'workshop', country = '', city = '', isFreeEvent = false, hideFacebookEvents = false, fromDate = "", toDate = "", onlyMyEvents = false)  {
        let parameters, countryProp, cityProp, isFreeEventProp, hideFacebookEventsProp, createdByProp;
        this.startFiltering$.emit();
        
        if(country != " "){
            countryProp = '&countryValue=' + country;
        } else countryProp = '';

        if(city != " "){
            cityProp = '&city=' + city;
        } else cityProp = '';

        if(isFreeEvent) {
            isFreeEventProp = '&isFreeEvent=' + isFreeEvent
        } else isFreeEventProp = '';

        if(hideFacebookEvents) {
            hideFacebookEventsProp = '&hideFacebookEvents=' + hideFacebookEvents
        } else hideFacebookEventsProp = '';

        if(onlyMyEvents){
            let currentUser = JSON.parse(localStorage.getItem("currentUser"));
            createdByProp = '&createdBy=' + currentUser.userId;
        } else createdByProp = '';
        
        parameters = '?type=' + type + '&fromDate=' + fromDate + '&toDate=' + toDate
                     + countryProp + isFreeEventProp + hideFacebookEventsProp + cityProp + createdByProp;

        return this.http.get(this.PORT + '/events/' + parameters)
            .toPromise()
            .then(res => {
                let events = res.json();
                /*for(let i = 0; i < events.length; i++) {
                    events[i].group = this.groupsService.getGroupDetails(events[i].group)
                }*/
                for(let i = 0; i < events.length; i++) {
                    if(events[i].destination.regularDays && events[i].destination.regularDays.length > 0){
                        events[i].days = this.convertDays(events[i].destination.regularDays)
                    }
                    if(events[i].guestsIdsList && events[i].guestsIdsList.length > 0){
                        console.log(events[i].guestsIdsList)
                        events[i].guests = this.usersService.getUsersDetails(events[i].guestsIdsList);
                    }
                }
                this.visibleEventsChanged$.emit(events)
            })
            .catch(this.handleError);
    }

    convertDays(regularDays){
        let days = [];
        for(let i = 0; i < regularDays.length; i++) {
            switch(regularDays[i]) { 
                case 0:  
                    days.push("Sunday");
                    break
                case 1:
                    days.push("Monday");
                    break  
                case 2:  
                    days.push("Tuesday");
                    break
                case 3:  
                    days.push("Wednesday");
                    break
                case 4:  
                    days.push("Thursday");
                    break
                case 5:  
                    days.push("Friday");
                    break
                case 6:  
                    days.push("Saturday");
                    break
              }
        }
        return days;
        
    }

    addEvent(newEvent, isConvert, facebookEventId)  {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({headers: headers});

        let currentUser = JSON.parse(localStorage.getItem("currentUser"));
        newEvent.createdBy = currentUser.userId;
        
        return this.http.post(this.PORT + '/events/add/' + isConvert + '/' + facebookEventId, JSON.stringify(newEvent), options)
            .toPromise()
            .then(res => {
                if(newEvent.mainImage != "oldImage"){
                    return this.uploadFile(newEvent.mainImage, res.json()._id);
                }           
            })
            .then(res => {
                this.eventCreated$.emit();
            })
            .catch(this.handleError);
    }

    updateEvent(event, updateEventId)  {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({headers: headers});

        let currentUser = JSON.parse(localStorage.getItem("currentUser"));
        event.createdBy = currentUser.userId;

        return this.http.post(this.PORT + '/events/update/' + updateEventId, JSON.stringify(event), options)
            .toPromise()
            .then(res => {
                if(event.mainImage != "oldImage"){
                    return this.uploadFile(event.mainImage, res.json()._id);
                }
            })
            .then(res => {
                this.eventCreated$.emit();
            })
            .catch(this.handleError);
    }

    deleteEvent(eventId)  {
        let that = this;
        return this.http.get(this.PORT + '/events/delete/' + eventId)
        .toPromise()
        .then(function(){
            that.messageService.add({severity:'success', summary:'Service Message', detail:'Event deleted succesfully'});
            that.eventDeleted$.emit();
        })
        .catch(this.handleError);
    }

    openDeletePopup(eventId){
        this.openDeletePopup$.emit(eventId);
    }

    openConfirmConvertPopup(){
        this.openConfirmConvertPopup$.emit();
    }

    convertEvent(){
        this.convertToMyEventConfirmed$.emit();
    }

    searchByEvents(visibleEvents){
        this.searchByEvents$.emit(visibleEvents);
    }

    searchByGuest(oldSearchValue, searchInDescription){
        this.searchByGuestEvents$.emit({searchValue: oldSearchValue, searchInDescription: searchInDescription});
    }

    uploadFile(fileToUpload: any, eventId) {
        return this.http.post(this.PORT + '/events/uploadFile/' + eventId, fileToUpload)
        .toPromise()
        .catch(this.handleError);
    }

   editEvent(event, type){
    this.editEvent$.emit({event: event, type: type});
   }

   createEvent(){
    this.createEvent$.emit();
   }

    private handleError(error: any): Promise<any> {
         console.error('Error', error);
         return Promise.reject(error.message || error);
    }

    public visibleEventsChanged(visibleEvents): void {
         this.visibleEventsChanged$.emit(visibleEvents);
    }
    
    public eventClicked(clickedEvent): void {
        if(clickedEvent._id){
            this.eventDetailsLoading$.emit();
            if(clickedEvent.isFacebookEvent){
                this.http.get(this.PORT + '/events/getFacebookEvent/' + clickedEvent._id)
                .toPromise()
                .then(res => {
                    clickedEvent = res.json();
                    this.eventClicked$.emit(clickedEvent);
                })
            } else {
                this.http.get(this.PORT + '/events/' + clickedEvent._id)
                .toPromise()
                .then(res => {
                    clickedEvent = res.json();
                    if(clickedEvent.destination.regularDays && clickedEvent.destination.regularDays.length > 0){
                        clickedEvent.days = this.convertDays(clickedEvent.destination.regularDays)
                    }
                    //clickedEvent.group = this.groupsService.getGroupDetails(clickedEvent.group)
                    clickedEvent.guests = this.usersService.getUsersDetails(clickedEvent.guestsIdsList);
                    this.eventClicked$.emit(clickedEvent);
                })
            }
            
        } else {
            this.eventClicked$.emit(clickedEvent);
        }
        
            
    }

    public markerClicked(clickedMarker): void {
        if(clickedMarker._id){
            this.eventDetailsLoading$.emit();
            if(clickedMarker.isFacebookEvent){
                this.http.get(this.PORT + '/events/getFacebookEvent/' + clickedMarker._id)
                .toPromise()
                .then(res => {
                    clickedMarker = res.json();
                    this.markerClicked$.emit(clickedMarker);
                })
            } else {
                this.http.get(this.PORT + '/events/' + clickedMarker._id)
                .toPromise()
                .then(res => {
                    clickedMarker = res.json();
                    if(clickedMarker.destination.regularDays && clickedMarker.destination.regularDays.length > 0){
                        clickedMarker.days = this.convertDays(clickedMarker.destination.regularDays)
                    }
                    //clickedMarker.group = this.groupsService.getGroupDetails(clickedMarker.group)
                    clickedMarker.guests = this.usersService.getUsersDetails(clickedMarker.guestsIdsList)
                    this.markerClicked$.emit(clickedMarker);
                })
            }
        } else {
            this.markerClicked$.emit(clickedMarker);
        }
   }

   public openMultyEventsPopup(multyEvents): void {
        this.openMultyEventsPopup$.emit(multyEvents);
    } 

}