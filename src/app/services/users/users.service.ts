import { EventEmitter, Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { MessageService } from 'primeng/components/common/messageservice';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class UsersService {
    PORT =  environment.production ? '' : 'http://localhost:3000';
    allUsers;
    public openUserPopup$;
    public openEditUserPopup$;
    public closeEditUserPopup$;
    public openChangePasswordPopup$;


    constructor(
        private http: Http,
        private messageService: MessageService,
    ){
        this.openUserPopup$ = new EventEmitter();
        this.openEditUserPopup$ = new EventEmitter();
        this.closeEditUserPopup$ = new EventEmitter();
        this.openChangePasswordPopup$ = new EventEmitter();
    }

    getUser(userId)  {
        let filteredUsersList = [];
        return this.http.get(this.PORT + '/users/' + userId)
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);
    }


    getUsers()  {
        let that = this;
        return this.http.get(this.PORT + '/users')
            .toPromise()
            .then(res => res.json())
            .then(function(res){
                that.allUsers = that.convertUsers(res);         
            })
            .catch(this.handleError);
    }

    getUsersDetails(usersIds = []){
        let filteredUsersList = [];
        this.allUsers.forEach(function (user) {
            for (let i = 0; i < usersIds.length; i++){
                if(!usersIds[i].fullName && user._id === usersIds[i]){
                    filteredUsersList.push(user);
                }
            }
        });
        for (let i = 0; i < usersIds.length; i++){
            if(usersIds[i].fullName){
                filteredUsersList.push(usersIds[i]);
            }
        }
        return filteredUsersList;
    }

    updateUser(user, userId){
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({headers: headers});
        return this.http.post(this.PORT + '/users/update/' + userId, JSON.stringify(user), options)
        .toPromise()
        .then(res => {
            if(res.json().errorStatus == "emailExist"){
                return false;
            } else {
                if(user.mainImage && user.mainImage != "oldImage"){
                    return this.uploadFile(user.mainImage, res.json()._id);
                } else {
                    return true;
                }
            }
            
        })
        .then(res => {
            if(res && user.name){
                this.messageService.add({severity:'success', summary:'Service Message', detail:'User updated succesfully'});
                this.getUsers();
                this.closeEditUserPopup$.emit(user);
            } else {
                return false;
            }
            
        })
        .catch(this.handleError);
    }

    uploadFile(fileToUpload: any, userId) {
        return this.http.post(this.PORT + '/users/uploadFile/' + userId, fileToUpload)
        .toPromise()
        .catch(this.handleError);
    }

    checkOldPassword(userId, oldPassword)  {
        return this.http.get(this.PORT + '/users/password/' + userId + '/' + oldPassword)
        .toPromise()
        .then(function(isValid){
            return isValid.json();
        })
        .catch(this.handleError);
    }

    convertUsers(users){
        for(let i = 0; i < users.length; i++) {
            if(users[i].grad){
                users[i].fullName = `${users[i].grad.toLowerCase() || ''} `;
            } else {
                users[i].fullName = '';
            }
            if(users[i].apelido){
                users[i].fullName = users[i].fullName + `${users[i].apelido || ''} (${users[i].name || ''} ${users[i].surname || ''}) `;
            } else {
                users[i].fullName = users[i].fullName + `${users[i].name || ''} ${users[i].surname || ''}`;
            }
            if((!users[i].country || !users[i].country.label) && users[i].group){
                users[i].fullName = users[i].fullName + `${users[i].group || ''}`;
            }
            if(users[i].country && users[i].country.label && users[i].group){
                users[i].fullName = users[i].fullName + `${users[i].group || ''}/${users[i].country.label || ''}`;
            }
            if(users[i].country && users[i].country.label && !users[i].group){
                users[i].fullName = users[i].fullName + `${users[i].country.label || ''}`;
            } 
        }
        return users;
    }


    /* Using for filtering drop-down list*/
    filterUsers(query, allUsers = this.allUsers) {
        let filtered : any[] = [];
        for(let i = 0; i < allUsers.length; i++) {
            let queryArr = query.split(' ');
            let user = allUsers[i];
            let counter = 0;
            for(let j = 0; j < queryArr.length; j++) {
                if( queryArr[j] === "" ||
                    user.name.toLowerCase().indexOf(queryArr[j].toLowerCase()) === 0 ||
                    (user.surname && user.surname.toLowerCase().indexOf(queryArr[j].toLowerCase()) === 0 )  ||
                    (user.grad && user.grad.toLowerCase().indexOf(queryArr[j].toLowerCase()) === 0) ||
                    (user.apelido && user.apelido.toLowerCase().indexOf(queryArr[j].toLowerCase()) === 0) ||
                    (user.country && user.country.label.toLowerCase().indexOf(queryArr[j].toLowerCase()) === 0) ||
                    (user.group && user.group.toLowerCase().indexOf(queryArr[j].toLowerCase()) != -1)) {
                    counter++;
                    if(counter == queryArr.length){
                        filtered.push(user);
                    }
                }
            }
            
            
        }
        return filtered;
      }

  

    private handleError(error: any): Promise<any> {
         console.error('Error', error);
         return Promise.reject(error.message || error);
    }

    public openUserPopup(userId): void {
        this.getUser(userId).then(user => {
            this.openUserPopup$.emit(user);
        }); 
    }   

    public openEditUserPopup(userId): void {
        this.getUser(userId).then((user) => this.openEditUserPopup$.emit(user))
    } 

    public openChangePasswordPopup(user): void {
         this.openChangePasswordPopup$.emit(user)
    }
  

}