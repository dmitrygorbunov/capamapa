import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CountriesService {
    allCountries;
    constructor(private http: Http){
    }

    getCountries()  {
        let that = this;
        return this.http.get('assets/data/countries.json')
            .toPromise()
            .then(countries => countries.json()) 
            .then(function(countries){
            let allCountries = countries.filter(function (country) {
                if (country.value && country.value !== 'NULL') {
                    return country;
                    } 
                });
                that.allCountries = allCountries;
            })  
            .catch(this.handleError);
    }
   
     /* Using for filtering drop-down list*/
     filterCountry(query, allCountries = this.allCountries) {
       let filtered : any[] = [];
       for(let i = 0; i < allCountries.length; i++) {
           let country = allCountries[i];
           if(country.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
               filtered.push(country);
           }
       }
       return filtered;
     }

    private handleError(error: any): Promise<any> {
         console.error('Error', error);
         return Promise.reject(error.message || error);
    }
}