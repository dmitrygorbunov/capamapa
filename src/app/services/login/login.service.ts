import { EventEmitter, Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { MessageService } from 'primeng/components/common/messageservice';
import { UsersService } from '../../services/users/users.service';
import { EventsService } from '../../services/events/events.service';
import { environment } from '../../../environments/environment';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginService {
    public toggleLoginPopup$;
    public openSignUpPopup$;
    public openForgotPasswordPopup$;    
    public loggedIn$;
    public loggedOut$;
    public passwordSent$;
    
    isFromSidebar = false;
    isConverting = false;
    PORT =  environment.production ? '' : 'http://localhost:3000';

    constructor(
        private http: Http,
        private messageService: MessageService,
        private usersService: UsersService,
        private eventsService: EventsService
    ){
        this.toggleLoginPopup$ = new EventEmitter();
        this.openSignUpPopup$ = new EventEmitter();
        this.openForgotPasswordPopup$ = new EventEmitter();        
        this.loggedIn$ = new EventEmitter();
        this.loggedOut$ = new EventEmitter();
        this.passwordSent$ = new EventEmitter();
    }

    checkUserExpireDate(){

        let currentUser = JSON.parse(localStorage.getItem("currentUser")) || {};
        let now = new Date();
        let expireDate = new Date(currentUser.expireDate);
        if(currentUser && expireDate.getTime() < now.getTime()) {
            localStorage.setItem("currentUser", JSON.stringify({}));
        }      

       /* return this.http.get(this.PORT + '/facebook')
        .toPromise()
        .then(user => {
        })
        .catch(this.handleError);*/

    }

    login(email, password, rememberMe)  {
        let parameters = '?email=' + email + '&password=' + password;
        return this.http.get(this.PORT + '/users/login' + parameters)
        .toPromise()
        .then(user => {
            this.logedIn(user.json(), rememberMe)
        })
        .catch(this.handleError);
    }

    logout(){
        localStorage.setItem("currentUser", JSON.stringify({}));
        this.loggedOut$.emit();
    }
    
    createUser(newUser)  {
        let that = this;
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({headers: headers});
        return this.http.post(this.PORT + '/users/add' , JSON.stringify(newUser), options)
            .toPromise()
            .then(function(user){
                if(user.json().errorStatus == "emailExist"){
                    return false;
                } else {
                    that.loggedIn$.emit({isSidebar: that.isFromSidebar, currentUser: user.json()});
                    that.usersService.getUsers();
                    let expireDate = that.addMinutes(120); // 2 hour
                    localStorage.setItem("currentUser", JSON.stringify({userEmail: user.json().email, userId: user.json()._id, expireDate: expireDate}));
                    return user.json();
                }
                
            })
            .catch(this.handleError);
    }
    
    getUserFromLocalStorage() {
        let currentUser = JSON.parse(localStorage.getItem("currentUser"));
        if(currentUser && currentUser.userId){
            this.usersService.getUser(currentUser.userId)
            .then((currentUser) => this.loggedIn$.emit({isSidebar: false, currentUser: currentUser}))
            .catch(function() {
                localStorage.setItem("currentUser", JSON.stringify({}))
            });
        }
    }

    public openForgotPasswordPopup() {
        this.openForgotPasswordPopup$.emit();
    } 

    public resetPassword(email) {
        let that = this;

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        const options = new RequestOptions({headers: headers});

        let body = {
            email: email,
            name: "",
            password: ""
        }

        return this.http.post(this.PORT + '/users/reset' , JSON.stringify(body), options)
        .toPromise()
        .then(function(user){
            if(user.json().errorStatus == "emailNotExist"){
                return false;
            } else {
                body.name = user.json().name;
                body.password = user.json().password;
                return that.http.post(that.PORT + '/emails/forgot-password' , JSON.stringify(body), options)
                .toPromise()
                .then(function(){
                    return true;
                })
            }
        })
        .catch(this.handleError);
    }

    public toggleLoginPopup(toggle, fromSidebar, isConverting) {
        this.toggleLoginPopup$.emit(toggle);
        this.isFromSidebar = fromSidebar;
        this.isConverting = isConverting;
    } 
    
    public openSignUpPopup() {
        this.openSignUpPopup$.emit();
    }

    public logedIn(user, rememberMe) {
        if(user.length > 0){
            if(this.isFromSidebar && !this.isConverting){
                console.log(222)
                this.eventsService.createEvent();
            }            
            this.loggedIn$.emit({isFromSidebar: this.isFromSidebar, isConverting: this.isConverting, currentUser: user[0]});
            this.toggleLoginPopup$.emit(false);
            this.messageService.add({severity:'success', summary:'Service Message', detail:'Welcome ' + user[0].name});
            if(rememberMe){
                let expireDate = this.addMinutes(10080); // 1 week
                localStorage.setItem("currentUser", JSON.stringify({userEmail: user[0].email, userId: user[0]._id, expireDate: expireDate}));
            }
            else {
                let expireDate = this.addMinutes(120); // 2 hours
                localStorage.setItem("currentUser", JSON.stringify({userEmail: user[0].email, userId: user[0]._id, expireDate: expireDate}));
            }
            return true;
        } else {
            localStorage.setItem("currentUser", JSON.stringify({}));
            return false;
        }
        
    } 

    private handleError(error: any): Promise<any> {
        console.error('Error', error);
        return Promise.reject(error.message || error);
   }

   addMinutes(minutes) {
       let date = new Date();
       return new Date(date.getTime() + minutes*60000);
    }

}