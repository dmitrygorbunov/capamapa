"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GmapFilterService = /** @class */ (function () {
    function GmapFilterService() {
        this.fromDateChanged$ = new core_1.EventEmitter();
        this.toDateChanged$ = new core_1.EventEmitter();
        this.countryChanged$ = new core_1.EventEmitter();
    }
    GmapFilterService.prototype.fromDateChanged = function (fromDate) {
        console.log(123);
        console.log(fromDate);
        this.fromDateChanged$.emit(fromDate);
    };
    GmapFilterService.prototype.toDateChanged = function (toDate) {
        console.log(234);
        console.log(toDate);
        this.toDateChanged$.emit(toDate);
    };
    GmapFilterService.prototype.countryChanged = function (country) {
        this.countryChanged$.emit(country);
    };
    return GmapFilterService;
}());
exports.GmapFilterService = GmapFilterService;
//# sourceMappingURL=gmap-filter.service.js.map