import { EventEmitter, Injectable } from '@angular/core';

export class GmapFilterService {
    public countryChanged$;
    public addressChanged$;
    public cityChanged$;
    public guestSearchChanged$;

    constructor(){
        this.countryChanged$ = new EventEmitter();
        this.addressChanged$ = new EventEmitter();
        this.cityChanged$ = new EventEmitter();
        this.guestSearchChanged$ = new EventEmitter();
    }    

    public countryChanged(country): void {
        this.countryChanged$.emit(country);
    }

    public cityChanged(lat, lng): void {
        this.cityChanged$.emit({lat: lat, lng: lng});
    }

    public addressChanged(lat, lng): void {
        this.addressChanged$.emit({lat: lat, lng: lng});
    }

    public guestSearchChanged(searchValue, searchInDescription): void {
        this.guestSearchChanged$.emit({searchValue: searchValue, searchInDescription: searchInDescription});
    }
}