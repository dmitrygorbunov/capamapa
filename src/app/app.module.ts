/* Angular modules */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule  } from '@angular/http';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { FormsModule, FormControl, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule,
         MatFormFieldModule,
         MatNativeDateModule,
         MatInputModule,
         MatIconModule,
         MatButtonToggleModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';

/* Work modules */
import { AppComponent } from './app.component';
import { EventsService } from './services/events/events.service';
import { GmapFilterService } from './services/gmap-filter/gmap-filter.service';
import { CountriesService } from './services/countries/countries.service';
//import { GroupsService } from './services/groups/groups.service';
import { UsersService } from './services/users/users.service';
import { LoginService } from './services/login/login.service';
import { GoogleMapComponent } from './components/google-map/google-map.component';
import { EventsSidebarComponent } from './components/events-sidebar/events-sidebar.component';
import { DetailsSidebarComponent } from './components/details-sidebar/details-sidebar.component';
import { AddEventSidebarComponent } from './components/add-event-sidebar/add-event-sidebar.component';
import { LeftSidebarComponent } from './components/left-sidebar/left-sidebar.component';
import { FilterPanelComponent } from './components/filter-panel/filter-panel.component';
import { UserPopupComponent } from './components/popup-user/user-popup.component';
import { EditUserPopupComponent } from './components/popup-edit-user/edit-user-popup.component';
import { MultyEventsPopupComponent } from './components/popup-multy-events/multy-events-popup.component';
import { LoginPopupComponent } from './components/popup-login/login-popup.component';
import { SignupPopupComponent } from './components/popup-signup/signup-popup.component';
import { ChangePassordPopupComponent } from './components/popup-change-password/change-password-popup.component';
import { DeleteEventPopupComponent } from './components/popup-delete-event/delete-event-popup.component';
import { ConvertEventPopupComponent } from './components/popup-convert-event/convert-event-popup.component';
import { ForgotPasswordPopupComponent } from './components/popup-forgot-password/forgot-password-popup.component';
import { MobilePanelComponent } from './components/mobile-panel/mobile-panel.component';

/* Addons */
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
//import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome'

/* PrimeNG modules */
import { ButtonModule }  from 'primeng/button';
import { SelectButtonModule } from 'primeng/selectbutton';
import { CalendarModule } from 'primeng/calendar';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ToolbarModule } from 'primeng/toolbar';
import { PanelModule } from 'primeng/panel';
import { SidebarModule } from 'primeng/sidebar';
import { CheckboxModule } from 'primeng/checkbox';
import { InputSwitchModule } from 'primeng/inputswitch';
import { LightboxModule } from 'primeng/lightbox';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FileUploadModule } from 'primeng/fileupload';
import { MessageModule } from 'primeng/message';
import { DialogModule } from 'primeng/dialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { GrowlModule } from 'primeng/growl';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  imports:      [ 
    BrowserModule,
    CommonModule,
    HttpModule,
    HttpClientModule,
    ButtonModule,
    SelectButtonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatButtonToggleModule,
    MatIconModule,
    Ng2PageScrollModule,
    CalendarModule,
    AutoCompleteModule,
    ToolbarModule,
    PanelModule,
    SidebarModule,
    CheckboxModule,
    InputSwitchModule,
    LightboxModule,
    InputTextModule,
    InputTextareaModule,
    FileUploadModule,
    MessageModule,
    DialogModule,
    ProgressSpinnerModule,
    GrowlModule,
    MenubarModule,
    LayoutModule,
    VirtualScrollerModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBiM_pNwp4bkyiaxYN6CY8yXq0UGy4TGKw',
      language: 'en',
      libraries: ["places"]
    }),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    AgmJsMarkerClustererModule
  ],
  declarations: [ 
    AppComponent,
    GoogleMapComponent,
    DetailsSidebarComponent,
    EventsSidebarComponent,
    AddEventSidebarComponent,
    LeftSidebarComponent,
    FilterPanelComponent,
    MobilePanelComponent,
    UserPopupComponent,
    EditUserPopupComponent,
    MultyEventsPopupComponent,
    LoginPopupComponent,
    SignupPopupComponent,
    ChangePassordPopupComponent,
    DeleteEventPopupComponent,
    ConvertEventPopupComponent,
    ForgotPasswordPopupComponent
  ],
  providers: [ EventsService, GmapFilterService, CountriesService, UsersService, /*GroupsService,*/ MessageService, LoginService ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
