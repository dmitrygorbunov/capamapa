import {AbstractControl} from '@angular/forms';
export class PasswordValidation {

    static MatchPassword(AC: AbstractControl) {
       let password = AC.get('password').value; // to get value in input tag
       let confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
        if(password != confirmPassword) {
            let control = AC.get('confirmPassword');
            if(control.errors){
              control.errors.MatchPassword = true;
              AC.get('confirmPassword').setErrors( control.errors )
            } else {
              AC.get('confirmPassword').setErrors( {MatchPassword: true} )
            }
            
        } else {
            return null
        }
    }
}