import { Component, Input, ElementRef, ViewChild} from '@angular/core';
import { UsersService } from '../../services/users/users.service';
import { CountriesService } from '../../services/countries/countries.service';
//import { GroupsService } from '../../services/groups/groups.service';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';

@Component({
  selector: 'edit-user-popup',
  templateUrl: './edit-user-popup.component.html'
})

export class EditUserPopupComponent {
  showPopup: boolean = false;
  loading = false;
  selectedUser = {
    name: "",
    surname: "",
    email: "",
    _id: "",
    apelido: "",
    grad: "",
    country: {
      label: "",
      value: ""
    },
    city: "",
    group: "",
    sitesList: [],
    image: "",
    description: ""
  };
  images = [];
  userForm: FormGroup;
  allCountries = [];
  filteredCountries = [];
  selectedCountry = '';
  allGroups = [];
  filteredGroups = [];
  selectedGroup = '';
  sitesList = [];
  sitevalidationError = false;
  emailExistError = false;

  @ViewChild("searchCity")
  public searchCityElementRef: ElementRef;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private usersService: UsersService,
    private countriesService: CountriesService,
    //private groupsService: GroupsService,
    private fb: FormBuilder
  ) {

    this.userForm = this.fb.group({
      'userName': new FormControl('', Validators.compose([
        Validators.required, 
        Validators.minLength(3),
        Validators.pattern('[-;:A-Za-z0-9() \u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff_-|,.<>"`]*')
      ])),
      'userSurname': new FormControl('', Validators.compose([
        Validators.pattern('[-;:A-Za-z0-9() \u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff_-|,.<>"`]*')
      ])),
      'userEmail': new FormControl('', Validators.compose([
        Validators.required, 
        Validators.email
      ])),
      'groupName': new FormControl('', Validators.compose([
        //Validators.required, 
        Validators.minLength(3),
        Validators.pattern('[-;:A-Za-z0-9() \u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff_-|,.<>"`]*')
      ])),
      'country': new FormControl('', Validators.compose([])),
      'city': new FormControl('', Validators.compose([])),
      'graduation': new FormControl('', Validators.compose([
        Validators.minLength(3),
        Validators.pattern('[-;:A-Za-z0-9() \u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff_-|,.<>"`]*')
      ])),
      'apelido': new FormControl('', Validators.compose([
        Validators.pattern('[-;:A-Za-z0-9() \u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff_-|,.<>"`]*')
      ])),
      'mainImage': new FormControl('', Validators.compose([])),
      'siteName': new FormControl([], Validators.compose([])),
      'userDescription': new FormControl([], Validators.compose([]))
    })
  }

  ngOnInit() {
    this.emailExistError = false;
    this.usersService.openEditUserPopup$.subscribe((user) => this.openEditUserPopup(user));    
    this.usersService.closeEditUserPopup$.subscribe(() => this.closeEditUserPopup());    
  }

  openEditUserPopup(user){
    this.userForm.reset();
    this.showPopup = true;
    this.sitevalidationError = false;
    this.emailExistError = false;
    this.selectedUser = user;
    this.userForm.controls.userName.setValue(this.selectedUser.name);
    this.userForm.controls.userSurname.setValue(this.selectedUser.surname);
    this.userForm.controls.graduation.setValue(this.selectedUser.grad);
    this.userForm.controls.apelido.setValue(this.selectedUser.apelido);
    this.userForm.controls.userEmail.setValue(this.selectedUser.email);
    this.userForm.controls.country.setValue(this.selectedUser.country);
    this.userForm.controls.groupName.setValue(this.selectedUser.group);
    this.userForm.controls.userDescription.setValue(this.selectedUser.description);
    this.userForm.controls.mainImage.setValue("oldImage");

    this.sitesList = this.selectedUser.sitesList;
    
    if(this.selectedUser.country && this.selectedUser.country.value){
      this.initCityGoogleAutocomplete(this.selectedUser.country.value);
      this.userForm.controls.city.setValue(this.selectedUser.city);
    }
  }

  addImageToForm(event) {
    var formData: any = new FormData();
    formData.append('photo', event.files[0]);
    this.userForm.controls.mainImage.setValue(formData);
  }

  closeEditUserPopup(){
    this.showPopup = false;
  }

  changePassword(){
    this.showPopup = false;
    this.usersService.openChangePasswordPopup(this.selectedUser);
  }

  filterGroups(query) {
    //this.filteredGroups = this.groupsService.filterGroups(query);
  }

  selectGroup(group) {
    this.selectedGroup = group.value;
  }

  filterCountry(query) {
    this.filteredCountries = this.countriesService.filterCountry(query);
  }

  selectCountry(country) {
    this.selectedCountry = country.value;
    this.userForm.controls.city.setValue("");
    this.initCityGoogleAutocomplete(country.value);
  }

  initCityGoogleAutocomplete(countryCode = "") {
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchCityElementRef.nativeElement, {
        types: ['(cities)'],
        componentRestrictions: {country: countryCode} // we can use only country for autocmplete options
      });

      autocomplete.addListener("place_changed", () => {
        let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        this.userForm.controls.city.setValue(place.address_components[0].long_name);        
      });
    });
  } 

  addSite(){
    if(this.userForm.controls.siteName.value && this.userForm.controls.siteName.value.length > 6){
      this.sitesList.push(this.userForm.controls.siteName.value); 
      this.userForm.controls.siteName.setValue("");
      this.sitevalidationError = false;
    } else {
      this.sitevalidationError = true;
    }
  }

  removeSite(index){
    this.sitesList.splice(index, 1);
  }

  onSubmit(value){
    this.userForm.controls.city.setValue(this.searchCityElementRef.nativeElement.value);
    if (this.userForm.valid) {
      this.loading = true;
      let that = this;

      this.usersService.updateUser({
        name: this.userForm.controls.userName.value,
        surname: this.userForm.controls.userSurname.value,
        apelido: this.userForm.controls.apelido.value,
        grad: this.userForm.controls.graduation.value,
        country: this.userForm.controls.country.value,
        city: this.userForm.controls.city.value,
        group: this.userForm.controls.groupName.value,
        email: this.userForm.controls.userEmail.value,
        description: this.userForm.controls.userDescription.value,
        mainImage: this.userForm.controls.mainImage.value,
        sitesList: this.sitesList
      }, this.selectedUser._id)
      .then(function(res) {
        if(res){
          that.loading = false;
          that.userForm.reset();
        } else {
          that.loading = false;
          that.emailExistError = true;
        }
        
      });
     
    } else {
      // validate all form fields
      Object.keys(this.userForm.controls).forEach(field => { 
        const control = this.userForm.get(field);            
        control.markAsTouched({ onlySelf: true });       
      });
    }
  }


} 