import { Component, Input, Output, EventEmitter} from '@angular/core';

//import { GoogleMapComponent } from './google-map.component';
import { EventsService } from '../../services/events/events.service';
import { UsersService } from '../../services/users/users.service';
import { LoginService } from '../../services/login/login.service';

@Component({
  selector: 'details-sidebar',
  templateUrl: './details-sidebar.component.html'
})

export class DetailsSidebarComponent {
  
  selectedEvent = {_id: "", createdBy: "", name: "", isFacebookEvent: false, description: "", guests: []};
  savedEvent = {_id: "", createdBy: "", name: "", isFacebookEvent: false, description: ""};
  loading = false;
  editable = false;
  loggedIn = false;
  searchValue;
  searchInDescription = true;
  @Output() editEventBtnClick = new EventEmitter();
  @Output() isEditable = new EventEmitter();

  constructor(
    private eventsService: EventsService,
    private usersService: UsersService,
    private loginService: LoginService,
  ) {}

  ngOnInit() {
    this.eventsService.eventClicked$.subscribe(selectedEvent => this.updateEventDetails(selectedEvent)); 
    this.eventsService.markerClicked$.subscribe(selectedEvent => this.updateEventDetails(selectedEvent));
    this.eventsService.eventDetailsLoading$.subscribe(() => this.loading = true);  
    this.eventsService.convertToMyEventConfirmed$.subscribe(() => this.convertToMyEventConfirmed());  
    this.eventsService.searchByGuestEvents$.subscribe(res => this.searchByGuest(res.searchValue, res.searchInDescription));
    this.loginService.loggedIn$.subscribe(res => {this.loggedInFunc(res.isConverting)});
    this.loginService.loggedOut$.subscribe(res => this.loggedOutFunc());
  }

  updateEventDetails(selectedEvent){
    this.selectedEvent = selectedEvent;
    this.savedEvent = selectedEvent;
    this.searchByGuest(this.searchValue, this.searchInDescription);
    this.editable = this.isMyEvent();
    if(this.selectedEvent.isFacebookEvent){
      this.isEditable.emit(this.selectedEvent.isFacebookEvent);
    } else this.isEditable.emit(this.editable);
    
    this.loading = false;
  }

  searchByGuest(searchValue, searchInDescription){
    this.searchValue = searchValue;
    this.searchInDescription = searchInDescription;
    this.selectedEvent = JSON.parse(JSON.stringify(this.savedEvent));
    if(searchValue && searchValue.length > 3){
      if(searchInDescription && this.selectedEvent.description && this.selectedEvent.description.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1){
        let index = this.selectedEvent.description.toLowerCase().indexOf(searchValue.toLowerCase())
        let string1 = this.selectedEvent.description.substring(0, index);
        let string2 = this.selectedEvent.description.substring(index + searchValue.length);
        this.selectedEvent.description = string1 + '<b class="green-background">' + searchValue + '</b>' + string2;
      }
      if(this.selectedEvent.guests){
        for (let j = 0; j < this.selectedEvent.guests.length; j++) {
          if(this.selectedEvent.guests[j].fullName && this.selectedEvent.guests[j].fullName.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1){
            let index = this.selectedEvent.guests[j].fullName.toLowerCase().indexOf(searchValue.toLowerCase())
            let string1 = this.selectedEvent.guests[j].fullName.substring(0, index);
            let string2 = this.selectedEvent.guests[j].fullName.substring(index + searchValue.length);
            this.selectedEvent.guests[j].fullName = string1 + '<b class="green-background">' + searchValue + '</b>' + string2;
          }
        }
      }      
    } 
  }

  openUserPopup(userId){
    this.usersService.openUserPopup(userId);
  }

  deleteEvent(userId){
    this.eventsService.openDeletePopup(this.selectedEvent._id);
  }

  editEvent(){
    this.editEventBtnClick.emit();
    this.eventsService.editEvent(this.selectedEvent, "edit");
  }

  convertToMyEvent(){
    this.eventsService.openConfirmConvertPopup();
  }

  convertToMyEventConfirmed(){
    if(this.loggedIn){
      this.editEventBtnClick.emit();
      this.eventsService.editEvent(this.selectedEvent, "convert");
    } else{
      this.loginService.toggleLoginPopup(true, true, true);
    }
  }

  loggedInFunc(isConverting){
    this.loggedIn = true;
    if(isConverting){
      this.editEventBtnClick.emit();
      this.eventsService.editEvent(this.selectedEvent, "convert");
    }
  }

  loggedOutFunc(){
    this.loggedIn = false;
  }

  isMyEvent(){
    let currentUser = localStorage.getItem("currentUser");
    if(currentUser && this.selectedEvent.createdBy && JSON.parse(currentUser).userId == this.selectedEvent.createdBy){
      return true;
    } else return false;
  }

} 