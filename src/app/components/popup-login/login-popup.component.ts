import { Component, Input} from '@angular/core';
import { LoginService } from '../../services/login/login.service';

@Component({
  selector: 'login-popup',
  templateUrl: './login-popup.component.html'
})

export class LoginPopupComponent {
  showPopup: boolean = false;
  selectedUser = {};
  images = [];
  email;
  password;
  loginError = false;
  rememberMe = false;

  constructor(
    private loginService: LoginService
  ) {
    
  }

  ngOnInit() {
    this.loginService.toggleLoginPopup$.subscribe(toggle => this.toggleLoginPopup(toggle));
  }

  toggleLoginPopup(toggle){
    this.loginError = false;
    this.showPopup = toggle;
  }

  login(){
    this.loginService.login(this.email, this.password, this.rememberMe)
    .then(isSuccess => {
        this.loginError = !isSuccess;
    });
  }

  signUp(){
    this.toggleLoginPopup(false);
    this.loginService.openSignUpPopup();
  }

  forgotPassword(){
    this.toggleLoginPopup(false);
    this.loginService.openForgotPasswordPopup();
  }

} 