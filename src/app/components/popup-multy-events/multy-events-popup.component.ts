import { Component, Input, Output, EventEmitter} from '@angular/core';
import { EventsService } from '../../services/events/events.service';

@Component({
  selector: 'multy-events-popup',
  templateUrl: './multy-events-popup.component.html'
})

export class MultyEventsPopupComponent {
  showPopup: boolean = false;
  multyEvents = [];

  @Output() mapMarkerClick = new EventEmitter();

  constructor(
    private eventsService: EventsService
  ) {
    
  }

  ngOnInit() {
    this.eventsService.openMultyEventsPopup$.subscribe(multyEvents => this.openMultyEventsPopup(multyEvents));
  }

  openMultyEventsPopup(multyEvents){
    this.multyEvents = multyEvents;
    this.showPopup = true;
  }

  eventClick(selectedEvent) {
    this.eventsService.markerClicked(selectedEvent);
    this.mapMarkerClick.emit(selectedEvent);
    this.showPopup = false;
  }


} 