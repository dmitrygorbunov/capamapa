import { Component, Input} from '@angular/core';
import { LoginService } from '../../services/login/login.service';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'forgot-password-popup',
  templateUrl: './forgot-password-popup.component.html'
})

export class ForgotPasswordPopupComponent {
  showPopup = false;
  passwordSent = false;
  loading = false;
  emailError = false;
  userForm: FormGroup;

  constructor(
    private loginService: LoginService,
    private fb: FormBuilder
  ) {
    this.userForm = this.fb.group({
      'userEmail': new FormControl('', Validators.compose([
          Validators.required,
          Validators.email
      ]))
    })
  }

  ngOnInit() {
    this.loginService.openForgotPasswordPopup$.subscribe(() => this.openForgotPasswordPopup());
    this.loginService.passwordSent$.subscribe(() => this.passwordSentFunc());
  }

  onSubmit(form){
    if (this.userForm.valid){
      let that = this;
      this.loading = true;
      this.loginService.resetPassword(form.userEmail)
      .then(function(success){
        if(success){
          that.passwordSent = true;
          that.loading = false;
        } else {
          that.emailError = true;
          that.loading = false;
        }
      });
    } else {
      // validate all form fields
      Object.keys(this.userForm.controls).forEach(field => { 
        const control = this.userForm.get(field);            
        control.markAsTouched({ onlySelf: true });       
      });
    }
    
  }

  openForgotPasswordPopup(){ 
    this.userForm.reset();  
    this.passwordSent = false; 
    this.emailError = false;
    this.showPopup = true;
  }

  passwordSentFunc(){
    this.passwordSent = true;
    this.loading = false;
  }
} 