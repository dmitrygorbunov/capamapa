"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var events_service_1 = require("../shared/events.service");
var gmap_filter_service_1 = require("../shared/gmap-filter.service");
var GoogleMapComponent = /** @class */ (function () {
    /* private mapsAPILoader: MapsAPILoader
   
     constructor(private eventsService: EventsService) {
         this.events = [];
         console.log(333)
         console.log(this.toDate)
     } */
    function GoogleMapComponent(eventsService, gmapFilterService) {
        this.eventsService = eventsService;
        this.gmapFilterService = gmapFilterService;
        this.mapMarkerClick = new core_1.EventEmitter();
        this.title = 'My first angular2-google-maps project';
        this.globalLat = 49.9927326;
        this.globalLng = 36.23085022;
        this.visibleEventsCounter = 0;
        this.fromDate = new Date;
        this.toDate = new Date;
        this.events = [];
    }
    GoogleMapComponent.prototype.selectFromDate = function (fromDate) {
        console.log('fromDate ' + fromDate);
        this.fromDate = fromDate;
    };
    GoogleMapComponent.prototype.selectToDate = function (toDate) {
        console.log('toDate ' + toDate);
        this.toDate = toDate;
    };
    GoogleMapComponent.prototype.selectCountry = function (country) {
        if (country.CountryName) {
            this.country = country;
            this.globalLat = +country.CapitalLatitude;
            this.globalLng = +country.CapitalLongitude;
        }
        else {
            this.country = country;
        }
    };
    GoogleMapComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.country = {};
        this.gmapFilterService.fromDateChanged$.subscribe(function (fromDate) { return _this.selectFromDate(fromDate); });
        this.gmapFilterService.toDateChanged$.subscribe(function (toDate) { return _this.selectToDate(toDate); });
        this.setCurrentPosition();
        this.getEvents();
        this.gmapFilterService.countryChanged$.subscribe(function (country) { return _this.selectCountry(country); });
    };
    GoogleMapComponent.prototype.getEvents = function () {
        var _this = this;
        this.eventsService.getEvents().then(function (events) {
            _this.events = events;
            console.log('Get Events Done');
            console.log(_this.events);
        });
    };
    Object.defineProperty(GoogleMapComponent.prototype, "filteredEvents", {
        get: function () {
            //  console.log(this.events)
            var fromDate = this.fromDate, toDate = this.toDate, country = this.country, visibleEventsCounter = 0;
            this.events.map(function (event) {
                // console.log(toDate)
                if (new Date(event.destination.fromDate) >= fromDate &&
                    new Date(event.destination.toDate) <= toDate) {
                    if (country.CountryName) {
                        if (event.destination.country == country.CountryName) {
                            event.visible = true;
                            visibleEventsCounter += 1;
                            return event;
                        }
                        else
                            event.visible = false;
                        return event;
                    }
                    else {
                        event.visible = true;
                        visibleEventsCounter += 1;
                        return event;
                    }
                }
                else
                    event.visible = false;
                return event;
            });
            if (this.visibleEventsCounter != visibleEventsCounter) {
                this.visibleEventsCounter = visibleEventsCounter;
                this.eventsService.visibleEventsChanged(this.visibleEventsCounter);
            }
            return this.events;
        },
        enumerable: true,
        configurable: true
    });
    GoogleMapComponent.prototype.setCurrentPosition = function () {
        var _this = this;
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                _this.globalLat = position.coords.latitude;
                _this.globalLng = position.coords.longitude;
            });
        }
    };
    GoogleMapComponent.prototype.markerClick = function (selectedEvent) {
        console.log('Click');
        console.log(this.toDate);
        console.log(selectedEvent);
        this.mapMarkerClick.emit(selectedEvent);
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], GoogleMapComponent.prototype, "mapMarkerClick", void 0);
    GoogleMapComponent = __decorate([
        core_1.Component({
            selector: 'google-map',
            templateUrl: './app/components/google-map.component.html'
        }),
        __metadata("design:paramtypes", [events_service_1.EventsService,
            gmap_filter_service_1.GmapFilterService])
    ], GoogleMapComponent);
    return GoogleMapComponent;
}());
exports.GoogleMapComponent = GoogleMapComponent;
//# sourceMappingURL=google-map.component.js.map