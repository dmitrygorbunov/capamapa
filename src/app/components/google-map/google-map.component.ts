import { Component, OnInit, Output, Input, OnChanges, EventEmitter, ViewChild  } from '@angular/core';

import { EventsService } from '../../services/events/events.service';
import { GmapFilterService } from '../../services/gmap-filter/gmap-filter.service';
import { FilterPanelComponent } from '../filter-panel/filter-panel.component';
import {BreakpointObserver } from '@angular/cdk/layout';


import { AgmMap } from '@agm/core';

@Component({
  selector: 'google-map',
  templateUrl: './google-map.component.html'
})

export class GoogleMapComponent implements OnInit {
  visibleEvents;
  fromDate;
  toDate;
  country;
  visibleEventsCounter: number;
  @ViewChild(AgmMap) myMap: AgmMap;
  @Input() shouldClearSelectedEvent;
  @Output() mapMarkerClick = new EventEmitter();

  globalLat: number = 49.8268587;
  globalLng: number = 23.9922592;
  globalZoom: number = 3;
  showNewEventIcon: boolean = false;
  newEventLat: number;
  newEventLng: number;
  loading: boolean = true;
  selectedIconUrl = "../../assets/images/map-icon.png";
  facebookIconUrl = "../../assets/images/map-icon-facebook.png";

 /* private mapsAPILoader: MapsAPILoader

  constructor(private eventsService: EventsService) {
      this.events = [];
  } */

  constructor(
    private eventsService: EventsService,
    private gmapFilterService: GmapFilterService,
    public breakpointObserver: BreakpointObserver
  ) {
    this.visibleEventsCounter = 0;
    this.fromDate = new Date;
    this.toDate = new Date; 
    this.toDate.setMonth(this.toDate.getMonth() + 1);
    this.visibleEvents = [];
  }

  ngOnInit() {
    this.country = {};
    this.setCurrentPosition();
    this.eventsService.visibleEventsChanged$.subscribe(visibleEvents => this.updateGoogleMap(visibleEvents));
    this.eventsService.searchByEvents$.subscribe(visibleEvents => this.updateGoogleMap(visibleEvents));
    this.eventsService.eventClicked$.subscribe(selectedEvent => this.selectEvent(selectedEvent));
    this.eventsService.markerClicked$.subscribe(selectedEvent => this.selectEventFromMap(selectedEvent));
    this.eventsService.eventCreated$.subscribe(() => this.eventCreated());
    this.gmapFilterService.countryChanged$.subscribe(country => this.selectCountry(country));
    this.gmapFilterService.addressChanged$.subscribe(address => this.selectAddress(address));
    this.gmapFilterService.cityChanged$.subscribe(city => this.selectCity(city));
    if (this.breakpointObserver.isMatched('(max-width: 570px)')) {
      this.globalZoom = 3;
    }
  }

  ngOnChanges(changes) {
    if(changes.shouldClearSelectedEvent.currentValue){
      this.clearSelectedEvent();
    }
  }

  clearSelectedEvent(){
    this.showNewEventIcon = false;
    this.visibleEvents.map((event) => {
      if(event.isFacebookEvent){
        event.iconUrl = this.facebookIconUrl;
      } else event.iconUrl = "";
      this.myMap.triggerResize();
      return event;
    });
  }
  
  selectFromDate(fromDate) {
    this.fromDate = fromDate;
  }

  selectToDate(toDate) {
    this.toDate = toDate;
  }

  selectCountry(country) {
    if(country.CapitalLatitude && country.CapitalLongitude){
      this.country = country;
      this.globalLat = Number(country.CapitalLatitude);
      this.globalLng = Number(country.CapitalLongitude);
      this.globalZoom = 6;
      this.myMap.triggerResize();
    } else { this.country = country; }
  }

  selectAddress(address) {
    if(address.lat && address.lng){
      this.globalLat = Number(address.lat);
      this.globalLng = Number(address.lng);
      this.newEventLat = Number(address.lat);
      this.newEventLng = Number(address.lng);
      this.globalZoom = 11;
      this.showNewEventIcon = true;
      
      this.myMap.triggerResize();
    } 
  }

  
  selectCity(city) {
    if(city.lat && city.lng){
      this.globalLat = Number(city.lat);
      this.globalLng = Number(city.lng);
      this.globalZoom = 11;
      this.showNewEventIcon = false;
      this.myMap.triggerResize();
    } 
  }

  eventCreated() {
    this.showNewEventIcon = false;
    this.newEventLat = 0;
    this.newEventLng = 0;
  }

  selectEvent(event) {
    let that = this;
    this.showNewEventIcon = false;
    if(event.destination){
      this.globalLat = Number(event.destination.latitude);
      this.globalLng = Number(event.destination.longitude);
      this.globalZoom = 6;
      this.myMap.triggerResize();
    } 
    this.visibleEvents = this.visibleEvents.map(function(thisEvent) {
      if(event._id === thisEvent._id){
        if(thisEvent.iconUrl === that.selectedIconUrl){
          if(thisEvent.isFacebookEvent){
            thisEvent.iconUrl = that.facebookIconUrl;
          } else thisEvent.iconUrl = "";
        } else {
          thisEvent.iconUrl = that.selectedIconUrl;
        } 
      } else {
        if(thisEvent.isFacebookEvent){
          thisEvent.iconUrl = that.facebookIconUrl;
        } else thisEvent.iconUrl = "";
      }
      that.myMap.triggerResize();
      return thisEvent;
    });
  }

  selectEventFromMap(event) {
    let that = this;
    this.showNewEventIcon = false;
    this.visibleEvents = this.visibleEvents.map(function(thisEvent) {
      if(event._id === thisEvent._id){
        if(thisEvent.iconUrl === that.selectedIconUrl){
          if(thisEvent.isFacebookEvent){
            thisEvent.iconUrl = that.facebookIconUrl;
          } else thisEvent.iconUrl = "";
        } else {
          thisEvent.iconUrl = that.selectedIconUrl;
        } 
      } else {
        if(thisEvent.isFacebookEvent){
          thisEvent.iconUrl = that.facebookIconUrl;
        } else thisEvent.iconUrl = "";
      }
      return thisEvent;
    });
  }

  /*getEvents() {
    this.eventsService.getEvents('workshop').then( events => {
       this.events = events;
    });
  }*/

  updateGoogleMap(visibleEvents) {
    this.showNewEventIcon = false;
    this.visibleEvents = visibleEvents.map((event) => {
      if(event.isFacebookEvent){
        event.iconUrl = this.facebookIconUrl;
      } else event.iconUrl = "";
      this.myMap.triggerResize();
      return event;
    });
    
  }
 

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.globalLat = position.coords.latitude;
        this.globalLng = position.coords.longitude;
        if (this.breakpointObserver.isMatched('(max-width: 570px)')) {
          this.globalZoom = 4;
        } else {
          this.globalZoom = 5;
        }
      });
    } 
    this.loading = false;
  }

  markerClick(clickedMarker) {
    this.showNewEventIcon = false;
    let duplicated = [];
    this.visibleEvents.forEach(function(event) {
      if(clickedMarker.destination.latitude == event.destination.latitude &&
         clickedMarker.destination.longitude == event.destination.longitude ){
          duplicated.push(event);
      }
    });

    if(duplicated.length > 1){
      console.log('Open multy events popup')
      this.eventsService.openMultyEventsPopup(duplicated);   
    } else {
      console.log('Open regular event')
      //clickedMarker.iconUrl = this.selectedIconUrl;    // for faster icon change
      this.eventsService.markerClicked(clickedMarker);
      this.mapMarkerClick.emit(clickedMarker);
    }    
  }

} 