import { Component, Input, Output, EventEmitter} from '@angular/core';
import { UsersService } from '../../services/users/users.service';

@Component({
  selector: 'left-sidebar',
  templateUrl: './left-sidebar.component.html'
})

export class LeftSidebarComponent {

  myId = '5cb99c5a0d1d200017a8e3ee';

  constructor(
    private usersService: UsersService
  ) {}

  userLinkClicked(){
    this.usersService.openUserPopup(this.myId);
  }

} 