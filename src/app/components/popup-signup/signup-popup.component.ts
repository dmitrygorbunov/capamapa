import { Component, Input} from '@angular/core';
import { LoginService } from '../../services/login/login.service';
import { UsersService } from '../../services/users/users.service';
import { PasswordValidation } from '../../directives/password-validation';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'signup-popup',
  templateUrl: './signup-popup.component.html'
})

export class SignupPopupComponent {
  showPopup: boolean = false;
  selectedUser = {};
  images = [];
  name;
  email;
  password;
  confirmPassword;
  userForm: FormGroup;
  loading = false;
  submitClicked = false;
  emailExistError = false;

  constructor(
    private loginService: LoginService,
    private userService: UsersService,
    private fb: FormBuilder,
    private messageService: MessageService
  ) {
    this.userForm = this.fb.group({
      'userName': new FormControl('', Validators.compose([
          Validators.required, 
          Validators.minLength(3),
          Validators.pattern('[-;:A-Za-z0-9() \u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff_-|,.<>"`]*')
      ])),
      'userEmail': new FormControl('', Validators.compose([
          Validators.required,
          Validators.email
      ])),
      'password': new FormControl('', Validators.compose([
          Validators.required, 
          Validators.minLength(5)
      ])),
      'confirmPassword': new FormControl('', Validators.compose([
          Validators.required, 
          Validators.minLength(5)
      ]))
    }, {
      validator: PasswordValidation.MatchPassword
    })
  }

  ngOnInit() {
    this.emailExistError = false;
    this.loginService.openSignUpPopup$.subscribe(() => this.openSignUpPopup());
  }

  openSignUpPopup(){
    this.showPopup = true;
  }

  onSubmit(value){
    
    this.submitClicked = true;
    if (this.userForm.valid) {
      let that = this;
      this.loading = true;
      this.loginService.createUser({
        name: this.userForm.controls.userName.value,
        email: this.userForm.controls.userEmail.value,
        password: this.userForm.controls.password.value,
      })
      .then(function(user) { 
        if(user){
          that.loading = false;
          that.showPopup = false;
          that.userForm.reset();
          that.messageService.add({severity:'success', summary:'Service Message', detail:'New user added succesfully'});
          that.userService.openEditUserPopup(user._id);
        } else {
          that.loading = false;
          that.emailExistError = true;
        }
       
       });
    }
  }


} 