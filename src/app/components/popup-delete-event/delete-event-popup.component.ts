import { Component, Input} from '@angular/core';
import { EventsService } from '../../services/events/events.service';


@Component({
  selector: 'delete-event-popup',
  templateUrl: './delete-event-popup.component.html'
})

export class DeleteEventPopupComponent {
  showPopup: boolean = false;
  selectedEventId;

  constructor(
    private eventsService: EventsService
  ) {
  }

  ngOnInit() {
    this.eventsService.openDeletePopup$.subscribe((eventId) => this.openDeletePopup(eventId));    
  }

  openDeletePopup(eventId){
    this.showPopup = true;
    this.selectedEventId = eventId;
  }

  deleteEvent(){
    this.eventsService.deleteEvent(this.selectedEventId);
    this.showPopup = false;
  }

} 