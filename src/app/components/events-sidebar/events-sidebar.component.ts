import { Component, OnChanges, Inject, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { EventsService } from '../../services/events/events.service';
import { GmapFilterService } from '../../services/gmap-filter/gmap-filter.service';
import { DOCUMENT} from '@angular/common';
import { PageScrollConfig, PageScrollService, PageScrollInstance } from 'ng2-page-scroll';
import { LoginService } from '../../services/login/login.service';
import { VirtualScrollerComponent } from 'ngx-virtual-scroller';

@Component({
  selector: 'events-sidebar',
  templateUrl: './events-sidebar.component.html'
})

export class EventsSidebarComponent implements OnChanges {
  visibleEvents = [];
  savedEvents = [];
  savedSearchByGuestsEvents = [];
  savedSearchByGroupEvents = [];
  selectedEvent = { _id:null };
  loggedIn = false;
  loading = true;
  searchInDescription = true;
  searchValue = "";
  oldSearchValue = "";
  @Input() shouldClearSelectedEvent;
  @Output() sidebarEventClick = new EventEmitter();
  @Output() addEventBtnClick = new EventEmitter();
  @Output() closeSidebar = new EventEmitter();
  @ViewChild('eventsList') eventsListRef: ElementRef;
  @ViewChild(VirtualScrollerComponent)
    private virtualScroller: VirtualScrollerComponent;

  constructor(
    private eventsService: EventsService,
    private pageScrollService: PageScrollService,
    private loginService: LoginService,
    private gmapFilterService: GmapFilterService,
    @Inject(DOCUMENT) private document: any
  ) {
    PageScrollConfig.defaultDuration = 300;
  }

  ngOnInit() {
    this.eventsService.visibleEventsChanged$.subscribe(visibleEvents => this.updateEventsList(visibleEvents));   
    this.eventsService.markerClicked$.subscribe(selectedEvent => this.updateSelectedEvent(selectedEvent));
    this.eventsService.startFiltering$.subscribe(() => this.startFiltering());
    this.loginService.loggedIn$.subscribe(res => {this.loggedInFunc(res.isFromSidebar)});
    this.loginService.loggedOut$.subscribe(res => this.loggedOutFunc());
    this.gmapFilterService.guestSearchChanged$.subscribe(res => this.searchByGuest(res.searchValue, res.searchInDescription));
  }

  ngOnChanges(changes) {
    if(changes.shouldClearSelectedEvent.currentValue){
      this.clearSelectedEvent();
    }
  }

  searchByName(){
    if(this.searchValue.length > 3){
      let filteredEvents = [];
      let events = JSON.parse(JSON.stringify(this.savedSearchByGuestsEvents));
      for (let i = 0; i < events.length; i++) {
        events[i].name = events[i].name.replace('<b class="green-background">',"");
        events[i].name = events[i].name.replace('</b>',"");
        if(events[i].name.toLowerCase().indexOf(this.searchValue.toLowerCase()) !== -1 || (events[i].group && events[i].group.toLowerCase().indexOf(this.searchValue.toLowerCase()) !== -1)){
          if(events[i].name.toLowerCase().indexOf(this.searchValue.toLowerCase()) !== -1){
            let index = events[i].name.toLowerCase().indexOf(this.searchValue.toLowerCase())
            let string1 = events[i].name.substring(0, index);
            let string2 = events[i].name.substring(index + this.searchValue.length);
            events[i].name = string1 + '<b class="green-background">' + this.searchValue + '</b>' + string2;
          }
          if(events[i].group && events[i].group.toLowerCase().indexOf(this.searchValue.toLowerCase()) !== -1){
            let index = events[i].group.toLowerCase().indexOf(this.searchValue.toLowerCase())
            let string1 = events[i].group.substring(0, index);
            let string2 = events[i].group.substring(index + this.searchValue.length);
            events[i].group = string1 + '<b class="green-background">' + this.searchValue + '</b>' + string2;
          }
          filteredEvents.push(events[i]);
        }
      };
      if(filteredEvents.length > 0){
        this.visibleEvents = filteredEvents;
        this.savedSearchByGroupEvents = filteredEvents;
      } else {
        if(this.visibleEvents.length == this.savedSearchByGuestsEvents.length){
          this.visibleEvents = [];
          this.savedSearchByGroupEvents = [];
        }
      }
    } else {
      this.savedSearchByGroupEvents = this.savedEvents;
      if(this.savedSearchByGuestsEvents.length != this.savedEvents.length){
        this.searchByGuest(this.oldSearchValue, this.searchInDescription);
      } else this.visibleEvents = this.savedEvents;
      
    }
    this.eventsService.searchByEvents(this.visibleEvents);
  }

  searchByGuest(searchValue, searchInDescription){
    this.searchInDescription = searchInDescription;
    if(searchValue.length > 3){
      this.oldSearchValue = searchValue;
      let filteredEvents = [];
      let events = JSON.parse(JSON.stringify(this.savedSearchByGroupEvents));
      for (let i = 0; i < events.length; i++) {
        if(searchInDescription && events[i].description && events[i].description.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1){
          filteredEvents.push(events[i]);
        } else if (events[i].guests) {
          for (let j = 0; j < events[i].guests.length; j++) {
            if(events[i].guests[j].fullName && events[i].guests[j].fullName.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1){
              filteredEvents.push(events[i]);
              break;
            }
          }
        }        
      };
      if(filteredEvents.length > 0){
        this.visibleEvents = filteredEvents;
        this.savedSearchByGuestsEvents = filteredEvents;
      } else {
        if(this.visibleEvents.length == this.savedSearchByGroupEvents.length){
          this.visibleEvents = [];
          this.savedSearchByGuestsEvents = [];
        }
      }
    } else {
      this.oldSearchValue = "";
      this.savedSearchByGuestsEvents = this.savedEvents;
      if(this.savedSearchByGroupEvents.length != this.savedEvents.length){
        this.searchByName();
      } else this.visibleEvents = this.savedEvents;
    }
    this.eventsService.searchByGuest(this.oldSearchValue, this.searchInDescription);
    this.eventsService.searchByEvents(this.visibleEvents);
  }

  clearSearch(){
    this.searchValue = "";
    this.savedSearchByGroupEvents = this.savedEvents;
    this.searchByGuest(this.oldSearchValue, this.searchInDescription);
    this.eventsService.searchByEvents(this.visibleEvents);
  }

  updateEventsList(visibleEvents) {
    this.visibleEvents = visibleEvents;
    this.savedEvents = visibleEvents;
    this.savedSearchByGroupEvents = this.savedEvents;
    this.savedSearchByGuestsEvents = this.savedEvents;
    this.selectedEvent = { _id:null };
    this.loading = false;
    this.closeSidebar.emit();
    this.searchByName();
    this.searchByGuest(this.oldSearchValue, this.searchInDescription);
    if(this.virtualScroller){
      this.virtualScroller.scrollToIndex(0);
    }    
  }

  startFiltering() {
    this.loading = true;   
  }

  eventClick(selectedEvent) {
    if(this.selectedEvent._id != selectedEvent._id){
      this.selectedEvent = selectedEvent;
    } else this.selectedEvent = {_id:null};
    this.eventsService.eventClicked(this.selectedEvent);
    this.sidebarEventClick.emit(selectedEvent);
  }

  updateSelectedEvent(selectedEvent){
    if(this.selectedEvent._id != selectedEvent._id){
      let that = this;
       this.selectedEvent = selectedEvent;
       
      this.visibleEvents.forEach(function(element, index) {
        if(element._id == selectedEvent._id){
          that.virtualScroller.scrollToIndex(index);
        }
      });
    } else this.selectedEvent = {_id:null};
  }

  clearSelectedEvent(){
    this.selectedEvent = {_id:null};
  }

  addEventBtnClicked(){
    if(this.loggedIn){
      this.addEventBtnClick.emit();
      this.eventsService.createEvent();
      this.clearSelectedEvent();
    } else{
      this.loginService.toggleLoginPopup(true, true, false);
    }
  }

  loggedInFunc(isFromSidebar){
    this.loggedIn = true;
    if(isFromSidebar){
      this.addEventBtnClick.emit();
      this.clearSelectedEvent();
    }
  }

  loggedOutFunc(){
    this.loggedIn = false;
  }

} 