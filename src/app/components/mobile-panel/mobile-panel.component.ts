import { Component, ElementRef, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'mobile-panel',
  templateUrl: './mobile-panel.component.html'
})

export class MobilePanelComponent implements OnInit {
 
  @Output() showMobileMapBtnClick = new EventEmitter();
  @Output() displayLeftSidebarBtnClick = new EventEmitter();
  @Output() showEventsListBtnClick = new EventEmitter();
  @Output() showFiltersBtnClick = new EventEmitter();

  constructor() {}

  ngOnInit() {
      
  }

  showMobileMap() {
    this.showMobileMapBtnClick.emit();
  }

  displayLeftSidebar() {
    this.displayLeftSidebarBtnClick.emit();
  }

  showEventsList() {
    this.showEventsListBtnClick.emit();
  }

  showFilters() {
    this.showFiltersBtnClick.emit();
  }
  

} 