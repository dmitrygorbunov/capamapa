import { Component, Input} from '@angular/core';
import { UsersService } from '../../services/users/users.service';

@Component({
  selector: 'user-popup',
  templateUrl: './user-popup.component.html'
})

export class UserPopupComponent {
  showPopup: boolean = false;
  selectedUser = {image: "", grad: "", apelido: "", name: "", surname: "", group: "", description: "", country: {label: ""}, sitesList: []};
  images = [];

  constructor(
    private usersService: UsersService
  ) {}

  ngOnInit() {
    this.usersService.openUserPopup$.subscribe(user => this.openUserPopup(user));
  }

  openUserPopup(user){
    this.selectedUser = user;
    this.showPopup = true;
  }


} 