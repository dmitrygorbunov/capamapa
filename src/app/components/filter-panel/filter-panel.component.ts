/// <reference types="@types/googlemaps" />
import { Component, ElementRef, Input, Output, EventEmitter, ViewChild, NgZone, OnInit} from '@angular/core';
import { FormControl } from "@angular/forms";
import { MapsAPILoader } from '@agm/core';
import { MenuItem } from 'primeng/api';
import {BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

//import { GoogleMapComponent } from './google-map.component';
import { EventsService } from '../../services/events/events.service';
import { CountriesService } from '../../services/countries/countries.service';
import { GmapFilterService } from '../../services/gmap-filter/gmap-filter.service';
import { LoginService } from '../../services/login/login.service';
import { UsersService } from '../../services/users/users.service';


@Component({
  selector: 'filter-panel',
  templateUrl: './filter-panel.component.html'
})

export class FilterPanelComponent implements OnInit {
  fromDate;
  toDate;
  visibleEvents;
  allCountries = [];
  filteredCountries = [];
  displayCityFilter:boolean = false;
  eventTypes = [];
  selectedEventType = 'workshop';
  selectedCountry = ' ';
  //selectedCountryLabel = ' ';
  isFreeEvent = false;
  hideFacebookEvents = false;
  onlyMyEvents = false;
  loggedIn = false;
  items: MenuItem[];
  country;
  selectedCity = '';
  currentUser = {name: "undefined user", _id: ""};
  en: any;
  mobileView = false;
  searchInDescription = true;
  searchValue = "";

  public zoom: number;

  @ViewChild("searchCity")
  public searchElementRef: ElementRef;

  constructor(  
    private mapsAPILoader: MapsAPILoader,
    private gmapFilterService: GmapFilterService,
    private eventsService: EventsService,
    private countriesService: CountriesService,
    private loginService: LoginService,
    private usersService: UsersService,
    private ngZone: NgZone,
    public breakpointObserver: BreakpointObserver
  ) {
    this.visibleEvents = [],
    this.fromDate = new Date();
    this.toDate = new Date();
    this.toDate.setMonth(this.toDate.getMonth() + 3);
    this.eventTypes = [
      {label: 'Workshop', value: 'workshop', icon: 'fa fa-fw fa-workshop'},
      {label: 'Class', value: 'class', icon: 'fa fa-fw fa-class'},
      {label: 'Roda', value: 'roda', icon: 'fa fa-fw fa-roda'}
  ];
  
  }

  ngOnInit() {
      this.en = {
        firstDayOfWeek: 1,
        dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        dayNamesMin: ["Su","Mo","Tu","We","Th","Fr","Sa"],
        monthNames: [ "January ","February","March ","April ","May ","June ","July ","August ","September ","October ","November ","December " ],
        monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
        today: 'Today',
        clear: 'Clear'
      }
      this.eventsService.visibleEventsChanged$.subscribe(visibleEvents => this.selectVisibleEventsCounter(visibleEvents));
      this.eventsService.searchByEvents$.subscribe(visibleEvents => this.selectVisibleEventsCounter(visibleEvents));
      this.loginService.loggedIn$.subscribe((res) => this.loggedInFunc(res.currentUser));
      this.loginService.loggedOut$.subscribe((res) => this.loggedOutFunc());
      this.eventsService.eventCreated$.subscribe(() => this.getEventsList());
      this.eventsService.eventDeleted$.subscribe(() => this.getEventsList());
      this.loginService.getUserFromLocalStorage();
      this.initGoogleAutocomplete();
      this.getEventsList();

      this.items = [
        {
          label: this.currentUser.name,
          icon: 'material-icons settings',
          items: [
              {label: 'Edit user', icon: 'material-icons person', command: () => this.editUser()},
              {label: 'Logout', icon: 'material-icons exit_to_app', command: () => this.logout()}
          ]
      }
    ]

    this.breakpointObserver
      .observe(['(min-width: 570px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.mobileView = false;
        } else {
          this.mobileView = true;
        }
      });
  }

  searchByGuest(){
    this.gmapFilterService.guestSearchChanged(this.searchValue, this.searchInDescription)
  }

  shouldSearchInDescription(searchInDescription){
    this.searchInDescription = searchInDescription;
    this.gmapFilterService.guestSearchChanged(this.searchValue, this.searchInDescription);
  }

  clearSearch(){
    this.searchValue = "";
    this.gmapFilterService.guestSearchChanged(this.searchValue, this.searchInDescription)
  }

  loggedInFunc(user){
    this.loggedIn = true;
    this.onlyMyEvents = false;
    this.currentUser = user;
    this.items[0].label = user.name;
  }

  loggedOutFunc(){
    this.loggedIn = false;
    this.onlyMyEvents = false;
    this.getEventsList();
  }

  editUser() {
    this.usersService.openEditUserPopup(this.currentUser._id);
  }

  logout() {
    this.loginService.logout();
  }

  initGoogleAutocomplete(countryCode = "") {
      this.mapsAPILoader.load().then(() => {
        let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ['(cities)'],
          componentRestrictions: {country: countryCode} // we can use only country for autocmplete options
        });

        autocomplete.addListener("place_changed", () => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if(!place.address_components){
            this.selectedCity = "";
            this.getEventsList();
          } else {
            this.selectedCity = place.address_components[0].long_name;
            //set latitude and longitude
            let lat = place.geometry.location.lat();
            let lng = place.geometry.location.lng();
    
            this.gmapFilterService.addressChanged(lat, lng);
            this.getEventsList();
          }
          
        });
      });
  }

  /* Using for filtering drop-down list*/
  filterCountry(query) {
    this.filteredCountries = this.countriesService.filterCountry(query);
  }

  wordCount(str) { 
    return str.split(" ").length;
  }

  selectCountry(country) {
    this.selectedCountry = country.value;
    /*if(country.value != " "){
      if(this.wordCount(country.label) > 2){
        this.selectedCountryLabel = country.label.split(' ', 2)[0] + ' ' + country.label.split(' ', 2)[1];
      } else {
        this.selectedCountryLabel = country.label;
      }
    } else {
      this.selectedCountryLabel = " ";
    }*/
    
    if(country.value != ' ') {
      this.displayCityFilter = true;
    } else {
      this.displayCityFilter = false;
      this.selectedCity = "";
    }
    
    this.getEventsList();
    this.initGoogleAutocomplete(country.value);

    this.gmapFilterService.countryChanged(country);

   /* if(countryIndex != 0){
        this.gmapFilterService.countryChanged(this.countries[countryIndex - 1]);
    } else {
        this.gmapFilterService.countryChanged({});
    }*/
  }

  selectVisibleEventsCounter(visibleEvents) {
    this.visibleEvents = visibleEvents;
  }

  changeEventType(){
    this.getEventsList();
  }

  selectFromDate(newDate) {
   this.fromDate = newDate;
   this.getEventsList();
  }

  selectToDate(newDate) {
   this.toDate = newDate;
   this.getEventsList();
  }

  selectPrice(isFreeEvent){
    this.isFreeEvent = isFreeEvent;
    this.getEventsList();
  }

  selectFacebookEvents(hideFacebookEvents){
    this.hideFacebookEvents = hideFacebookEvents;
    this.getEventsList();
  }

  selectMyEvents(onlyMyEvents){
    this.onlyMyEvents = onlyMyEvents;
    this.getEventsList();
  }

  openLoginPopup(){
    this.loginService.toggleLoginPopup(true, false, false);
  }

  getEventsList(){
    this.eventsService.getEvents(this.selectedEventType, this.selectedCountry, this.selectedCity, this.isFreeEvent, this.hideFacebookEvents, this.fromDate.toISOString(), this.toDate.toISOString(), this.onlyMyEvents);
  }
  

} 