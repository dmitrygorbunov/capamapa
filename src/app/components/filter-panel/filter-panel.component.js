"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var core_2 = require("angular2-google-maps/core");
//import { GoogleMapComponent } from './google-map.component';
var events_service_1 = require("../shared/events.service");
var countries_service_1 = require("../shared/countries.service");
var gmap_filter_service_1 = require("../shared/gmap-filter.service");
var FilterSidebarComponent = /** @class */ (function () {
    function FilterSidebarComponent(mapsAPILoader, gmapFilterService, eventsService, countriesService, ngZone) {
        this.mapsAPILoader = mapsAPILoader;
        this.gmapFilterService = gmapFilterService;
        this.eventsService = eventsService;
        this.countriesService = countriesService;
        this.ngZone = ngZone;
        this.countries = [];
        this.displayCityFilter = false;
        this.visibleEventsCounter = 0,
            this.fromDate = new Date;
        this.toDate = new Date;
        this.toDate.setMonth(this.toDate.getMonth() + 1);
    }
    FilterSidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getCountries();
        console.log(111);
        console.log(this.countries);
        this.eventsService.visibleEventsChanged$.subscribe(function (visibleEventsCounter) { return _this.selectVisibleEventsCounter(visibleEventsCounter); });
        this.selectFromDate(this.fromDate);
        this.selectToDate(this.toDate);
        this.initGoogleAutocomplete();
    };
    FilterSidebarComponent.prototype.initGoogleAutocomplete = function (countryCode) {
        var _this = this;
        if (countryCode === void 0) { countryCode = ""; }
        this.searchControl = new forms_1.FormControl();
        this.mapsAPILoader.load().then(function () {
            var autocomplete = new google.maps.places.Autocomplete(_this.searchElementRef.nativeElement, {
                types: ['(cities)'],
                componentRestrictions: { country: countryCode }
            });
            autocomplete.addListener("place_changed", function () {
                //get the place result
                console.log(234);
                console.log(autocomplete.getPlace());
                var place = autocomplete.getPlace();
                //  let refreshMap = google.maps.event.trigger(map, 'resize');
                //set latitude and longitude
                // this.globalLat = place.geometry.location.lat();
                //  this.globalLng = place.geometry.location.lng();
            });
        });
    };
    FilterSidebarComponent.prototype.getCountries = function () {
        var _this = this;
        this.countriesService.getCountries().then(function (countries) {
            _this.countries = countries.filter(function (country) {
                if (country.value && country.value != 'NULL') {
                    return country;
                }
            });
        });
    };
    FilterSidebarComponent.prototype.selectCountry = function (country) {
        this.displayCityFilter = true;
        this.initGoogleAutocomplete(country.value);
        console.log(5555);
        console.log(country);
        this.gmapFilterService.countryChanged(country);
        /* if(countryIndex != 0){
             this.gmapFilterService.countryChanged(this.countries[countryIndex - 1]);
         } else {
             this.gmapFilterService.countryChanged({});
         }*/
    };
    FilterSidebarComponent.prototype.selectVisibleEventsCounter = function (visibleEventsCounter) {
        this.visibleEventsCounter = visibleEventsCounter;
    };
    FilterSidebarComponent.prototype.selectFromDate = function (newDate) {
        console.log('select fromDate');
        this.fromDate = newDate;
        this.gmapFilterService.fromDateChanged(newDate);
    };
    FilterSidebarComponent.prototype.selectToDate = function (newDate) {
        console.log('select toDate');
        this.toDate = newDate;
        this.gmapFilterService.toDateChanged(newDate);
    };
    __decorate([
        core_1.ViewChild("search"),
        __metadata("design:type", Object)
    ], FilterSidebarComponent.prototype, "searchElementRef", void 0);
    FilterSidebarComponent = __decorate([
        core_1.Component({
            selector: 'filter-sidebar',
            templateUrl: './app/components/filter-sidebar.component.html'
        }),
        __metadata("design:paramtypes", [core_2.MapsAPILoader,
            gmap_filter_service_1.GmapFilterService,
            events_service_1.EventsService,
            countries_service_1.CountriesService,
            core_1.NgZone])
    ], FilterSidebarComponent);
    return FilterSidebarComponent;
}());
exports.FilterSidebarComponent = FilterSidebarComponent;
//# sourceMappingURL=filter-sidebar.component.js.map