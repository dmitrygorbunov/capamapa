import { Component, Input, ElementRef, ViewChild, Output, EventEmitter} from '@angular/core';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
//import { GoogleMapComponent } from './google-map.component';
import { MapsAPILoader } from '@agm/core';
import { CountriesService } from '../../services/countries/countries.service';
import { GmapFilterService } from '../../services/gmap-filter/gmap-filter.service';
import { EventsService } from '../../services/events/events.service';
//import { GroupsService } from '../../services/groups/groups.service';
import { UsersService } from '../../services/users/users.service';
import { MessageService } from 'primeng/components/common/messageservice';
import {BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'add-event-sidebar',
  templateUrl: './add-event-sidebar.component.html'
})

export class AddEventSidebarComponent {
  selectedEvent = {};
  eventTypes = [];
  eventRegularity = [];
  eventForm: FormGroup;
  allCountries = [];
  filteredCountries = [];
  selectedCountry = '';
  filteredGroups = [];
  selectedGroup = '';
  allUsers = [];
  filteredUsers = [];
  guestsList = [];
  guestsIdsList = [];
  guestName = '';
  displayCityFilter:boolean = false;
  latitude;
  longitude;
  loading = false;
  sitesList = [];
  selectedFile: File = null;
  fd = new FormData();
  sitevalidationError = false;
  updateEventId = null;
  isEventOn = {};
  guestsValidationError = false;
  guestsLanguageError = false;
  regularDaysValidationError = false;
  currentDate = new Date;
  en: any;
  mobileView = false;
  convertToMyEvent = false;
  convertedEvent;

  week = [
    {dayName: "Monday", controlName: "isEventOnMonday", startTimeControl: "mondayStartTime", endTimeControl: "mondayEndTime"},
    {dayName: "Tuesday", controlName: "isEventOnTuesday", startTimeControl: "tuesdayStartTime", endTimeControl: "tuesdayEndTime"},
    {dayName: "Wednesday", controlName: "isEventOnWednesday", startTimeControl: "wednesdayStartTime", endTimeControl: "wednesdayEndTime"},
    {dayName: "Thursday", controlName: "isEventOnThursday", startTimeControl: "thursdayStartTime", endTimeControl: "thursdayEndTime"},
    {dayName: "Friday", controlName: "isEventOnFriday", startTimeControl: "fridayStartTime", endTimeControl: "fridayEndTime"},
    {dayName: "Saturday", controlName: "isEventOnSaturday", startTimeControl: "saturdayStartTime", endTimeControl: "saturdayEndTime"},
    {dayName: "Sunday", controlName: "isEventOnSunday", startTimeControl: "sundayStartTime", endTimeControl: "sundayEndTime"}
  ]

  @Output() shouldCancelEditing = new EventEmitter();

  @ViewChild("searchAddress")
  public searchAddressElementRef: ElementRef;
  @ViewChild("searchCity")
  public searchCityElementRef: ElementRef;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private countriesService: CountriesService,
    private gmapFilterService: GmapFilterService,
    private eventsService: EventsService,
    //private groupsService: GroupsService,
    private usersService: UsersService,
    private messageService: MessageService,
    private fb: FormBuilder,
    public breakpointObserver: BreakpointObserver
  ) {
    this.eventForm = this.fb.group({
      'eventName': new FormControl('', Validators.compose([
        Validators.required, 
        Validators.minLength(6),
        Validators.pattern('[ •&-;:A-Za-z0-9() \u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff_-|,.<>"`]*')
      ])),
      'selectedEventType': new FormControl('workshop', Validators.compose([
        Validators.required])),
      'groupName': new FormControl('', Validators.compose([
        //Validators.required,
        Validators.minLength(3),
        Validators.pattern('[ •&-;:A-Za-z0-9() \u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff_-|,.<>"`]*')])),
      'startDate': new FormControl('', Validators.compose([
        Validators.required
      ])),
      'endDate': new FormControl('', Validators.compose([
        Validators.required
      ])),
      'startTime': new FormControl('', Validators.compose([])),
      'endTime': new FormControl('', Validators.compose([])),

      'eventDescription': new FormControl('', Validators.compose([
        Validators.required
      ])),
      'contacts': new FormControl('', Validators.compose([])),
      'guestName': new FormControl('', Validators.compose([
        Validators.pattern('[ •&-;:A-Za-z0-9() \u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff_-|,.<>"`]*')
      ])),
      'price': new FormControl('', Validators.compose([])),
      'country': new FormControl('', Validators.compose([
        Validators.required
      ])),
      'city': new FormControl('', Validators.compose([
        Validators.required
      ])),
      'address': new FormControl('', Validators.compose([
       Validators.required
      ]
      )),
      'isFreeEvent': new FormControl('', Validators.compose([
        Validators.required])),
      'mainImage': new FormControl([], Validators.compose([
        Validators.required])),
      'siteName': new FormControl([], Validators.compose([])),   
      'oneTimeEvent': new FormControl([], Validators.compose([])),

      // controls for regular event
      'isEventOnMonday': new FormControl('', Validators.compose([])),
      'isEventOnTuesday': new FormControl('', Validators.compose([])),
      'isEventOnWednesday': new FormControl('', Validators.compose([])),
      'isEventOnThursday': new FormControl('', Validators.compose([])),
      'isEventOnFriday': new FormControl('', Validators.compose([])),
      'isEventOnSaturday': new FormControl('', Validators.compose([])),
      'isEventOnSunday': new FormControl('', Validators.compose([])),
      
      'mondayStartTime': new FormControl('', Validators.compose([])),
      'mondayEndTime': new FormControl('', Validators.compose([])),
      'tuesdayStartTime': new FormControl('', Validators.compose([])),
      'tuesdayEndTime': new FormControl('', Validators.compose([])),
      'wednesdayStartTime': new FormControl('', Validators.compose([])),
      'wednesdayEndTime': new FormControl('', Validators.compose([])),
      'thursdayStartTime': new FormControl('', Validators.compose([])),
      'thursdayEndTime': new FormControl('', Validators.compose([])),
      'fridayStartTime': new FormControl('', Validators.compose([])),
      'fridayEndTime': new FormControl('', Validators.compose([])),
      'saturdayStartTime': new FormControl('', Validators.compose([])),
      'saturdayEndTime': new FormControl('', Validators.compose([])),
      'sundayStartTime': new FormControl('', Validators.compose([])),
      'sundayEndTime': new FormControl('', Validators.compose([])),
    });

    this.eventTypes = [
      {label: 'Workshop', value: 'workshop', icon: 'fa fa-fw fa-workshop'},
      {label: 'Class', value: 'class', icon: 'fa fa-fw fa-class'},
      {label: 'Roda', value: 'roda', icon: 'fa fa-fw fa-roda'}
    ];

    this.eventRegularity = [
      {label: 'One time event', value: true},
      {label: 'Regular event', value: false}
    ];

  }

  ngOnInit() {
    this.en = {
      firstDayOfWeek: 1,
      dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      dayNamesMin: ["Su","Mo","Tu","We","Th","Fr","Sa"],
      monthNames: [ "January ","February","March ","April ","May ","June ","July ","August ","September ","October ","November ","December " ],
      monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
      today: 'Today',
      clear: 'Clear'
    }
    this.eventsService.editEvent$.subscribe(data => this.editEvent(data.event, data.type));
    this.eventsService.createEvent$.subscribe(event => this.createEvent());   

    this.breakpointObserver
      .observe(['(min-width: 570px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.mobileView = false;
        } else {
          this.mobileView = true;
        }
      });
  }

  createEvent(){
    this.loading = true;
    setTimeout(() => {
      this.eventForm.reset();
      this.eventForm.controls.selectedEventType.setValue("workshop");
      this.eventForm.controls.oneTimeEvent.setValue(true);
      this.convertToMyEvent = false;
      this.updateEventId = null;
      this.guestsList = [];
      this.guestsIdsList = [];
      this.sitesList = [];
      this.addOneTimeEventControlls();
      this.loading = false;
    }, 100);
    
  }

  editEvent(event, type){
    this.loading = true;

    setTimeout(() => {
      if(type == 'convert'){
        this.convertToMyEvent = true;
        this.convertedEvent = event;
      } else {
        this.convertedEvent = {};
        this.convertToMyEvent = false;
      }

      this.eventForm.reset();

      this.updateEventId = event._id;
      this.eventForm.controls.selectedEventType.setValue(event.type);
      this.eventForm.controls.eventName.setValue(event.name);
      this.eventForm.controls.groupName.setValue(event.group);
      this.eventForm.controls.price.setValue(event.price);
      this.eventForm.controls.eventDescription.setValue(event.description);
      this.eventForm.controls.contacts.setValue(event.contacts);
      this.eventForm.controls.isFreeEvent.setValue(event.isFreeEvent);
      this.eventForm.controls.country.setValue({value: event.destination.countryValue, label: event.destination.countryLabel});
      this.eventForm.controls.mainImage.setValue("oldImage");
      
      this.guestsList = event.guests || [];
      this.sitesList = event.sitesList;
      this.latitude = event.destination.latitude;
      this.longitude = event.destination.longitude;

      this.initCityGoogleAutocomplete(event.destination.countryValue);
      this.eventForm.controls.city.setValue(event.destination.city);

      this.initAddressGoogleAutocomplete(event.destination.countryValue, event.destination.latitude, event.destination.longitude);
      this.eventForm.controls.address.setValue(event.destination.address);

      if(event.isOneTimeEvent){
        this.addOneTimeEventControlls();
        this.eventForm.controls.startDate.setValue(new Date(event.destination.fromDate));
        this.eventForm.controls.endDate.setValue(new Date(event.destination.toDate));
      } else {
        this.removeOneTimeEventControlls();
        this.eventForm.controls.isEventOnSunday.setValue(event.destination.regularDays.includes(0));
        this.eventForm.controls.isEventOnMonday.setValue(event.destination.regularDays.includes(1));
        this.eventForm.controls.isEventOnTuesday.setValue(event.destination.regularDays.includes(2));
        this.eventForm.controls.isEventOnWednesday.setValue(event.destination.regularDays.includes(3));
        this.eventForm.controls.isEventOnThursday.setValue(event.destination.regularDays.includes(4));
        this.eventForm.controls.isEventOnFriday.setValue(event.destination.regularDays.includes(5));
        this.eventForm.controls.isEventOnSaturday.setValue(event.destination.regularDays.includes(6));

        let counter = 0;
        for(let i = 0; i < this.week.length; i++) {
          if(this.eventForm.controls[this.week[i].controlName].value && event.destination.regularTimes[counter]){
            if(event.destination.regularTimes[counter].startTime){
              this.eventForm.controls[this.week[i].startTimeControl].setValue(new Date(event.destination.regularTimes[counter].startTime));
            }
            if(event.destination.regularTimes[counter].endTime){
              this.eventForm.controls[this.week[i].endTimeControl].setValue(new Date(event.destination.regularTimes[counter].endTime));
            }
            counter++;
          }
        }
      }

      this.eventForm.controls.oneTimeEvent.setValue(event.isOneTimeEvent);
      this.loading = false;
    }, 100);    
  }

  onSubmit(value){
    if(!this.eventForm.controls.isFreeEvent.value){
      this.eventForm.controls.isFreeEvent.setValue(false);
    }

    this.validateGuestsList();

    if(!this.eventForm.controls.oneTimeEvent.value){
      this.validateRegularDays();
    } else this.regularDaysValidationError = false;

    if (this.eventForm.valid && !this.guestsValidationError && !this.regularDaysValidationError) {

      let that = this;
      if(this.guestsList.length > 0){
        that.guestsIdsList = [];
        this.guestsList.forEach(function(guest, index) {
            if(guest._id){
              that.guestsIdsList.push(guest._id);
            } else {
              that.guestsIdsList.push(guest);
            }
        });
      } 

      let eventObject: any = {
        type: this.eventForm.controls.selectedEventType.value,
        name: this.eventForm.controls.eventName.value,
        group: this.eventForm.controls.groupName.value,
        isFreeEvent: this.eventForm.controls.isFreeEvent.value,
        price: this.eventForm.controls.price.value,
        description: this.eventForm.controls.eventDescription.value,
        contacts: this.eventForm.controls.contacts.value,
        destination: {
          countryValue: this.eventForm.controls.country.value.value,
          countryLabel: this.eventForm.controls.country.value.label,
          city: this.eventForm.controls.city.value,
          address: this.eventForm.controls.address.value,
          latitude: this.latitude,
          longitude: this.longitude,
          //fromTime:
          //toTime:
        },
        mainImage: this.eventForm.controls.mainImage.value,
        guestsIdsList: this.guestsIdsList,
        sitesList: this.sitesList,
        isOneTimeEvent: this.eventForm.controls.oneTimeEvent.value
      }

      this.loading = true;

      if(this.eventForm.controls.oneTimeEvent.value){
        eventObject.destination.fromDate = this.eventForm.controls.startDate.value;
        eventObject.destination.toDate = this.eventForm.controls.endDate.value;
        eventObject.destination.fromTime = this.eventForm.controls.startTime.value;
        eventObject.destination.toTime = this.eventForm.controls.endTime.value;
      } else {
        eventObject.destination.regularDays = [];
        eventObject.destination.regularTimes = [];
        this.eventForm.controls.isEventOnMonday.value ? (eventObject.destination.regularDays.push(1), eventObject.destination.regularTimes.push({startTime: this.eventForm.controls.mondayStartTime.value, endTime: this.eventForm.controls.mondayEndTime.value})) : "";
        this.eventForm.controls.isEventOnTuesday.value ? (eventObject.destination.regularDays.push(2), eventObject.destination.regularTimes.push({startTime: this.eventForm.controls.tuesdayStartTime.value, endTime: this.eventForm.controls.tuesdayEndTime.value})) : "";
        this.eventForm.controls.isEventOnWednesday.value ? (eventObject.destination.regularDays.push(3), eventObject.destination.regularTimes.push({startTime: this.eventForm.controls.wednesdayStartTime.value, endTime: this.eventForm.controls.wednesdayEndTime.value})) : "";
        this.eventForm.controls.isEventOnThursday.value ? (eventObject.destination.regularDays.push(4), eventObject.destination.regularTimes.push({startTime: this.eventForm.controls.thursdayStartTime.value, endTime: this.eventForm.controls.thursdayEndTime.value})) : "";
        this.eventForm.controls.isEventOnFriday.value ? (eventObject.destination.regularDays.push(5), eventObject.destination.regularTimes.push({startTime: this.eventForm.controls.fridayStartTime.value, endTime: this.eventForm.controls.fridayEndTime.value})) : "";
        this.eventForm.controls.isEventOnSaturday.value ? (eventObject.destination.regularDays.push(6), eventObject.destination.regularTimes.push({startTime: this.eventForm.controls.saturdayStartTime.value, endTime: this.eventForm.controls.saturdayEndTime.value})) : "";
        this.eventForm.controls.isEventOnSunday.value ? (eventObject.destination.regularDays.push(0), eventObject.destination.regularTimes.push({startTime: this.eventForm.controls.sundayStartTime.value, endTime: this.eventForm.controls.sundayEndTime.value})) : "";
      }

      if(this.convertToMyEvent){
        eventObject.facebookId = this.convertedEvent.facebookId;
        eventObject.image = this.convertedEvent.image;
      } else {
        this.convertedEvent = {};
      }

      if(this.updateEventId && !this.convertToMyEvent){
        eventObject._id = this.updateEventId
        this.eventsService.updateEvent(eventObject, this.updateEventId)

        .then(function() { 
          that.eventForm.reset();
          that.loading = false;
          that.addOneTimeEventControlls();
          that.sitevalidationError = false;
          that.guestsValidationError = false;
          that.guestsLanguageError = false;
          that.regularDaysValidationError = false;
          that.messageService.add({severity:'success', summary:'Service Message', detail:'Event updated succesfully'});
         });

      } else {
        this.eventsService.addEvent(eventObject, this.convertToMyEvent, this.convertedEvent._id)
        .then(function() { 
          that.eventForm.reset();
          that.loading = false;
          that.addOneTimeEventControlls();
          that.sitevalidationError = false;
          that.guestsValidationError = false;
          that.guestsLanguageError = false;
          that.regularDaysValidationError = false;
          that.messageService.add({severity:'success', summary:'Service Message', detail:'New event added succesfully'});
         });
      }
      
    } else {
      // validate all form fields
      Object.keys(this.eventForm.controls).forEach(field => { 
        const control = this.eventForm.get(field);            
        control.markAsTouched({ onlySelf: true }); 
        this.validateGuestsList();      
      });
    }
  }

  onPriceChange(){
    /*if(this.eventForm.controls.isFreeEvent){
      this.eventForm.removeControl('price');
    } else {
      var ctrl = this.fb.control('', []);
      this.eventForm.addControl('price', ctrl);
    }*/
  }

  changeEventType(){
    switch(this.eventForm.controls.selectedEventType.value) {
      
      case 'workshop':  
        this.eventForm.controls.oneTimeEvent.setValue(true);
        this.addOneTimeEventControlls();
        break
    
      case 'class':  
        this.eventForm.controls.oneTimeEvent.setValue(false);
        this.removeOneTimeEventControlls();
        break

      case 'roda':  
        this.eventForm.controls.oneTimeEvent.setValue(true);
        this.addOneTimeEventControlls();
        break
    }
  }

  changeEventRegularity(){
    if(this.eventForm.controls.oneTimeEvent.value){
      this.addOneTimeEventControlls();
    } else {
      this.removeOneTimeEventControlls();
    }
  }

  addOneTimeEventControlls(){
    var ctrl = this.fb.control('', [Validators.required]);
    this.eventForm.addControl('startDate', ctrl);
    this.eventForm.addControl('endDate', ctrl);
    if(this.convertToMyEvent){
      this.eventForm.controls.startDate.setValue(new Date(this.convertedEvent.destination.fromDate));
      this.eventForm.controls.endDate.setValue(new Date(this.convertedEvent.destination.toDate));
    }
  }

  removeOneTimeEventControlls(){
    this.eventForm.removeControl('startDate');
    this.eventForm.removeControl('endDate');
  }

  addImageToForm(event) {
    var formData: any = new FormData();
    formData.append('photo', event.files[0]);
    this.eventForm.controls.mainImage.setValue(formData);
  }

  filterCountry(query) {
    this.filteredCountries = this.countriesService.filterCountry(query);
  }

  selectCountry(country) {
    this.selectedCountry = country.value;
    this.eventForm.controls.city.setValue("");
    this.eventForm.controls.address.setValue("");
    if(country.value != ' ') {
      this.displayCityFilter = true;
    } else this.displayCityFilter = false;

    this.initCityGoogleAutocomplete(country.value);

    this.gmapFilterService.countryChanged(country);
  }

  filterGroups(query) {
    //this.filteredGroups = this.groupsService.filterGroups(query);
  }

  selectGroup(group) {
    this.selectedGroup = group.value;
  }

  // Guests filters
   filterUsers(query) {

    this.filteredUsers = this.usersService.filterUsers(query);
    this.filteredUsers.sort( this.compareUsers );
    let tempArray = this.filteredUsers;
    let that = this;
    if(this.guestsList.length > 0){
      this.guestsList.forEach(function(guest){
        if(guest._id){
          tempArray.forEach(function(user, index){
            if(user._id == guest._id){
              that.filteredUsers.splice(index, 1);
            }
          })
        }
      })
    }
  }

  compareUsers( a, b ) {
      if(!a.grad){
        return 1;
      }
      
      if(!b.grad){
        return -1;
      }
      
      if (a.grad.toLowerCase() <  b.grad.toLowerCase() ){
        return -1;
      }

      if (a.grad.toLowerCase() > b.grad.toLowerCase() ){
        return 1;
      }

      return 0;
    
  }
  
  selectUser(user) {
    if(user._id){
      this.guestsList.push(user);
      this.guestsLanguageError = false;
      this.eventForm.controls.guestName.reset(); 
      this.validateGuestsList();
    } else {
      if(this.eventForm.controls.guestName.value && this.eventForm.controls.guestName.valid){
        this.guestsList.push({fullName: user});
        this.guestsLanguageError = false;
        this.eventForm.controls.guestName.reset(); 
        this.validateGuestsList();
      } else {
        this.guestsLanguageError = true;
      }
    }    
  }

  validateGuestsList(){
    if(this.guestsList.length > 0){
      this.guestsValidationError = false;
    } else this.guestsValidationError = true;
  }

  deleteUser(index) {
    this.guestsList.splice(index, 1);
  }

  validateRegularDays(){
    if(!this.eventForm.controls.isEventOnMonday.value && !this.eventForm.controls.isEventOnTuesday.value && !this.eventForm.controls.isEventOnWednesday.value && !this.eventForm.controls.isEventOnThursday.value && !this.eventForm.controls.isEventOnFriday.value && !this.eventForm.controls.isEventOnSaturday.value && !this.eventForm.controls.isEventOnSunday.value){
      this.regularDaysValidationError = true;
    } else this.regularDaysValidationError = false;
  }

  initCityGoogleAutocomplete(countryCode = "") {
      this.mapsAPILoader.load().then(() => {
        let autocomplete = new google.maps.places.Autocomplete(this.searchCityElementRef.nativeElement, {
          types: ['(cities)'],
          componentRestrictions: {country: countryCode} // we can use only country for autocmplete options
        });

        autocomplete.addListener("place_changed", () => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          this.eventForm.controls.city.setValue(place.address_components[0].long_name);
          this.eventForm.controls.address.setValue("");


         //set latitude and longitude
         this.latitude = place.geometry.location.lat();
         this.longitude = place.geometry.location.lng();

         this.gmapFilterService.addressChanged(this.latitude, this.longitude);
         this.initAddressGoogleAutocomplete(countryCode, this.latitude, this.longitude);

        });
      });
  }

  initAddressGoogleAutocomplete(countryCode = "", lat, lng) {
      this.mapsAPILoader.load().then(() => {
        let autocomplete = new google.maps.places.Autocomplete(this.searchAddressElementRef.nativeElement, {
          componentRestrictions: {country: countryCode}, // we can use only country for autocmplete options
          bounds:	new google.maps.LatLngBounds(new google.maps.LatLng(lat, lng)), // not sure how it works...
          types: ['address']
        });

        

        autocomplete.addListener("place_changed", () => {

          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //set latitude and longitude
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();

          this.eventForm.controls.address.setValue(place.formatted_address);

          this.gmapFilterService.addressChanged(this.latitude, this.longitude);
        });
      });
  }

  changeAddressString(){
    // this.eventForm.controls.address.setValue(null); 
  }

  changeCity(){
    this.eventForm.controls.city.setValue("");
  }

  addSite(){
    if(this.eventForm.controls.siteName.value.length > 6){
      this.sitesList.push(this.eventForm.controls.siteName.value); 
      this.eventForm.controls.siteName.setValue("");
      this.sitevalidationError = false;
    } else {
      this.sitevalidationError = true;
    }
    
  }

  removeSite(index){
    this.sitesList.splice(index, 1);
  }

  openUserPopup(userId){
    this.usersService.openUserPopup(userId);
  }

  cancelEditing(){
    this.shouldCancelEditing.emit();
  }

  selectTime(control){
    if(!this.eventForm.controls[control].value){
      let date = new Date;
      date.setHours(12, 0, 0);
      this.eventForm.controls[control].setValue(new Date(date));
    }    
  }

  selectDate(){
    if(this.eventForm.controls.startDate.value && !this.eventForm.controls.endDate.value){
      let date = new Date(this.eventForm.controls.startDate.value);
      this.eventForm.controls.endDate.setValue(date);
    }    
  }
  
} 