import { Component, Input} from '@angular/core';
import { EventsService } from '../../services/events/events.service';


@Component({
  selector: 'convert-event-popup',
  templateUrl: './convert-event-popup.component.html'
})

export class ConvertEventPopupComponent {
  showPopup: boolean = false;
  selectedEventId;

  constructor(
    private eventsService: EventsService
  ) {
  }

  ngOnInit() {
    this.eventsService.openConfirmConvertPopup$.subscribe((eventId) => this.openConvertPopup());    
  }

  openConvertPopup(){
    this.showPopup = true;
  }

  convertEvent(){
    this.eventsService.convertEvent();
    this.showPopup = false;
  }

} 