import { Component, Input} from '@angular/core';
import { UsersService } from '../../services/users/users.service';
import { PasswordValidation } from '../../directives/password-validation';
import { Validators, FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'change-password-popup',
  templateUrl: './change-password-popup.component.html'
})

export class ChangePassordPopupComponent {
  showPopup: boolean = false;
  selectedUser = {_id: "", password: ""};
  images = [];
  oldPassword;
  password;
  confirmPassword;
  userForm: FormGroup;
  loading = false;
  oldPasswordError = false;

  constructor(
    private userService: UsersService,
    private fb: FormBuilder,
    private messageService: MessageService
  ) {
    this.userForm = this.fb.group({
      'oldPassword': new FormControl('', Validators.compose([
        Validators.required, 
        Validators.minLength(5)
    ])),
      'password': new FormControl('', Validators.compose([
          Validators.required, 
          Validators.minLength(5)
      ])),
      'confirmPassword': new FormControl('', Validators.compose([
          Validators.required, 
          Validators.minLength(5)
      ]))
    }, {
      validator: PasswordValidation.MatchPassword
    })
  }

  ngOnInit() {
    this.userService.openChangePasswordPopup$.subscribe((user) => this.openChangePasswordpPopup(user));
  }

  openChangePasswordpPopup(user){
    this.showPopup = true;
    this.userForm.reset();
    this.selectedUser = user;
    this.oldPasswordError = false;
  }

  changeOldPassword(){
    this.oldPasswordError = false;
  }

  onSubmit(value){

    if (this.userForm.valid && !this.oldPasswordError) {
      let that = this;
      this.loading = true;

      this.userService.checkOldPassword(this.selectedUser._id, this.userForm.controls.oldPassword.value)
      .then(function(isValid) { 
        if(isValid){
          that.userService.updateUser({
            password: that.userForm.controls.password.value,
          }, that.selectedUser._id)
          .then(function() { 
            that.loading = false;
            that.showPopup = false;
            that.userForm.reset();
            that.messageService.add({severity:'success', summary:'Service Message', detail:'Password changed succesfully'});
           });
        } else {
          that.loading = false;
          that.oldPasswordError = true;
        }
      })
      
    } else{
      // validate all form fields
      Object.keys(this.userForm.controls).forEach(field => { 
        const control = this.userForm.get(field);            
        control.markAsTouched({ onlySelf: true });       
      });
    }
  }


} 