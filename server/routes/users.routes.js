const express = require('express');
const router = express.Router();
const multer = require('multer');
const cloudinary = require('cloudinary').v2;
const bcrypt = require('bcrypt');
let saltRounds = 10;
let imageUrl = "";
let cloudinaryFolder = "";

cloudinary.config({ 
  cloud_name: 'hpthsmvhd', 
  api_key: '588536174472257', 
  api_secret: 'HMvNJ417lyFp3BjAjsjcd2oEHJY' 
});

// Prod or test image folder
if(process.env.PORT){
  imageUrl = 'dist/assets/uploads/';
  cloudinaryFolder = "productionUsers";
} else {
  imageUrl = 'src/assets/uploads/';
  cloudinaryFolder = "developmentUsers";
} 

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, imageUrl)
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '.jpg') //Appending .jpg
  }
})

let User = require('../models/userModel');
const upload = multer({storage: storage}).single('photo');

/* /users */
router.route('/').get(function(req, res, next) {
  let query = {};

  let parameters = `name 
                    surname 
                    apelido 
                    group.groupName
                    country.label 
                    city
                    grad
                    group`;

  User.find(query, parameters, function (err, users){
    if(err){
      console.log('Error: got users');
      console.log(err);
    }
    else {
      console.log('Success: got users');
      res.json(users);
    }
  });
});


/* /users/update/:id */
router.route('/update/:id').post(function(req, res) {

  User.find({email: req.body.email}, function (err, users) {
    if(err){
      console.log('Error: update user');
      console.log(err);
      res.status(400).send("unable to save to database");
    }
    else {
      if(users.length > 1 || (users.length > 0 && users[0]._id != req.params.id)){        
        res.status(200).send({errorStatus: 'emailExist', errorMsg: "This email is already exist!"});
      } else {
        if(!req.body.country && !req.body.password){ req.body.country = {label: ""}}
        if(!req.body.group && !req.body.password){ req.body.group = ""}
        if(!req.body.surname && !req.body.password){ req.body.surname = ""}

        if(req.body.password) {
          bcrypt.genSalt(saltRounds, function(err, salt) {
              bcrypt.hash(req.body.password, salt, function(err, hash) {
                  // Store hash in your password DB.
                  req.body.password = hash;
                  updateUser();
              });
          });
        } else {
          updateUser();
        }

        function updateUser(){
          User.findByIdAndUpdate(req.params.id, req.body, function (err, user) {
            if(err || !user){
              console.log('Error: update user');
              console.log(err);
              res.status(500).send('Error: update user');
            }
            else {
              console.log('Success: update user');
              user.password = '';
              res.json(user);
            }
          });
        }

        
      }
    }
  })
});

/* Check old password */
router.route('/password/:id/:password').get(function(req, res) {
  let id = req.params.id;
  let password = req.params.password;
  User.findById(id, function (err, user) {
    if(err || !user){
      console.log('Error: got users');
      console.log(err);
      res.status(500).send('Error: got user');
    }
    else {
      if(password == user.password){
        res.json(true);
      } else {
        // Compare crypted passwords
        console.log('Success: got users');
        bcrypt.compare(password, user.password, function(err, result) {
            console.log('Success: password');
            console.log(result);
            res.json(result);
        });
      }
    }
  });
});

/* Upload image */
router.post('/uploadFile/:id', function (req, res, next) {
  var path = '';

  upload(req, res, function (err) {
      if (err) {
        // An error occurred when uploading
        return res.status(422).send("an Error occured")
      }  
      // No error occured.
      path = req.file.path;
      cloudinary.uploader.upload(path, 
        {
          folder: cloudinaryFolder,
          quality: "auto",
          fetch_format: "auto"
        },
        function(error, result) { 
        if(result){
          if(result.url.indexOf("https") == -1){
            result.url = result.url.replace("http","https");
          } 
          User.findByIdAndUpdate(req.params.id, {image: result.url}, function (err, event) {
            if(err || !event){
              console.log('Error: update user');
              console.log(err);
              res.status(500).send('Error: update image');
            }
            else {
              console.log('Success: update user');
              console.log(event);
              return res.send("Upload Completed for " + path); 
            }
          });
        }     
      })
  });     
})


/* Reset password  */
router.route('/reset').post(function (req, res) {
  User.findOne({email: req.body.email}, function (err, user) {
    if(err){
      res.status(400).send("Server error");
    }
    else {
      if(user){

        let newPassword = Math.random().toString(36).substring(7);
        let newHashPassword;

        console.log(user)
        bcrypt.genSalt(saltRounds, function(err, salt) {
          bcrypt.hash(newPassword, salt, function(err, hash) {
              // Store hash in your password DB.
              newHashPassword = hash;
              updateUser();
          });
        });
        

        function updateUser(){
          User.findByIdAndUpdate(user._id, {password: newHashPassword}, function (err, user) {
            if(err || !user){
              console.log('Error: update user');
              console.log(err);
              res.status(500).send('Error: update user');
            }
            else {
              console.log('Success: update user');
              user.password = newPassword;
              res.json(user);
            }
          });
        }


      } else {
        res.status(200).send({errorStatus: 'emailNotExist', errorMsg: "This email is not exist!"});
      }
    }
  });
});


/* /users/add */
router.route('/add').post(function (req, res) {
  
  User.findOne({email: req.body.email}, function (err, user) {
    if(err){
      console.log('Error: update user');
      console.log(err);
      res.status(400).send("unable to save to database");
    }
    else {
      if(user){
        res.status(200).send({errorStatus: 'emailExist', errorMsg: "This email is already exist!"});
      } else {

        if(!req.body.country){ req.body.country = {label: ""}}
        if(!req.body.group){ req.body.group = ""}
        if(!req.body.surname){ req.body.surname = ""}

        bcrypt.genSalt(saltRounds, function(err, salt) {
            bcrypt.hash(req.body.password, salt, function(err, hash) {
                // Store hash in your password DB.
                req.body.password = hash;
                let user = new User(req.body);

                user.save()
                .then(user => {
                  console.log('Success: add user');
                  user.password = '';
                  res.status(200).json(user);
                })
                .catch(err => {
                  console.log('Error: add user');
                  res.status(400).send("unable to save to database");
                });
            });
        });
        
        
      }
    }
  })
  
});

/* /users/login */
router.route('/login').get(function(req, res, next) {
  let query = {};
  let parameters = `name 
                    surname 
                    apelido 
                    grad 
                    country 
                    city
                    group
                    email
                    password`;  

  if(req.query.email){
    query.email = req.query.email;
  }
  /*if(req.query.password){
    query.password = req.query.password;
  }*/

  User.find(query, parameters, function (err, users){
    if(err){
      console.log('Error: login');
      console.log(err);
      res.json([]);
    }
    else {
      console.log('Success: login');
      if(users.length > 0){
        if(req.query.password == users[0].password){
          users[0].password = '';
          res.json(users);
        } else {
          // Compare crypted passwords
          bcrypt.compare(req.query.password, users[0].password, function(err, result) {
              console.log('Success: password');
              console.log(result);
              if(result){
                users[0].password = '';
                res.json(users);
              } else {
                console.log('Error: password');
                console.log(err);
                res.json([]);
              }
          });
        }
      } else {
        res.json(users);
      }      
    }
  });
});

/* /users/:id */
router.route('/:id').get(function(req, res) {
  User.findById(req.params.id, function (err, user) {
    if(err || !user){
      console.log('Error: got users');
      console.log(err);
      res.status(500).send('Error: got user');
    }
    else {
      console.log('Success: got users');
      console.log(user);
      user.password = '';
      res.json(user);
    }
  });
});

module.exports = router;