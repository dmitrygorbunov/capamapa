const express = require('express');
const router = express.Router();
const multer = require('multer');
const cloudinary = require('cloudinary').v2;
const graph = require('fbgraph');
const countries = require('../data/countries');
let imageUrl = "";
let cloudinaryFolder = "";

graph.setAccessToken('EAAhe4AEcZC3oBAJRkky6yc8vtMFBwLoLUnjcx0BZBZB0vJvBuuXZBEMEZAmIXAJbXcsG8s69b7xglX3VMSJ18RRZB5EABQMf1CxLAKNFW4ucEsRb23wyXJhigGuTZB403DbrUKf7rjEfvGe1oOAt8vUCsYNRQntrzZC3jjPRLnrha9bTaZAM10RaG');

cloudinary.config({
  cloud_name: 'hpthsmvhd',
  api_key: '588536174472257',
  api_secret: 'HMvNJ417lyFp3BjAjsjcd2oEHJY'
});

// Prod or test image folder
if(process.env.PORT){
   imageUrl = 'dist/assets/uploads/';
   cloudinaryFolder = "productionEvents";
} else {
   imageUrl = 'src/assets/uploads/';
   cloudinaryFolder = "developmentEvents";
}

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, imageUrl)
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '.jpg') //Appending .jpg
  }
})

const Event = require('../models/eventModel');
const FacebookEvent = require('../models/facebookEventModel');
const FacebookEventBlackList = require('../models/facebookEventBlackListModel');
const upload = multer({storage: storage}).single('photo');


/* Get events */
router.route('/').get(function(req, res) {

  let parameters = `name
                    image
                    type
                    group
                    isOneTimeEvent
                    destination.fromDate
                    destination.toDate
                    destination.countryLabel
                    destination.city
                    destination.latitude
                    destination.longitude
                    destination.regularDays
                    description
                    guestsIdsList`;

  let options = { };

  let query = {
    type: req.query.type
  };

  if(req.query.isFreeEvent){
    query.isFreeEvent = req.query.isFreeEvent;
  }

  if(req.query.createdBy){
    query.createdBy = req.query.createdBy;
    runQuery();
  } else {
    if(req.query.countryValue){
      query['destination.countryValue'] = req.query.countryValue;
    }

    if(req.query.city){
      query['destination.city'] = req.query.city;
    }

    switch(req.query.type) {
      case 'workshop':
        query['destination.fromDate'] = {$lte: req.query.toDate};
        query['destination.toDate'] = {$gte: req.query.fromDate};
        options = {sort: {'destination.fromDate': 1}}
        runQuery();
        break

      case 'class':
        let days = [];
        let endDate = new Date(req.query.toDate);
        let workDate = new Date(req.query.fromDate);
        validateDate();

        function validateDate(){
          if(days.length < 7 && workDate <= endDate){
            days.push(workDate.getDay());
            workDate = new Date(workDate.setDate(workDate.getDate()+1));
            validateDate();
          }
        }

        query['destination.regularDays'] = {$in: days};
        runQuery();
        break

      case 'roda':
        // search one-time roda events
        query['destination.fromDate'] = {$lte: req.query.toDate};
        query['destination.toDate'] = {$gte: req.query.fromDate};
        query.isOneTimeEvent = true;
        let fullResult = [];
        runGetRodaQuery()
        .then(function(oneTimeEvents){
          fullResult = fullResult.concat(oneTimeEvents);

          // search regular roda events
          let rodaDays = [];
          let rodaEndDate = new Date(req.query.toDate);
          let rodaWorkDate = new Date(req.query.fromDate);

          validateRodaDate();

          function validateRodaDate(){
            if(rodaDays.length < 7 && rodaWorkDate <= rodaEndDate){
              rodaDays.push(rodaWorkDate.getDay());
              rodaWorkDate = new Date(rodaWorkDate.setDate(rodaWorkDate.getDate()+1));
              validateRodaDate();
            }
          }

          query['destination.fromDate'] = "";
          query['destination.toDate'] = "";
          query['destination.regularDays'] = {$in: rodaDays};
          query.isOneTimeEvent = false;

          runGetRodaQuery()
          .then(function(regularEvents){
            fullResult = fullResult.concat(regularEvents);
            res.json(fullResult);
          })

        });

        function runGetRodaQuery(){
          return Event.find(query, parameters, function (err, events) {
            if(err){
              console.log('Error: got events');
              console.log(err);
              res.json(err);
            }
            else {
              console.log('Success: got events');
              return events;
            }
          });
        }
        break
    }
  }

  function runQuery(){
    Event.find(query, parameters, options, function (err, events) {
      if(err){
        console.log('Error: got events');
        console.log(err);
        res.json(err);
      }
      else {
        console.log('Success: got events');
        if(!req.query.hideFacebookEvents){
          FacebookEvent.find(query, {}, options, function (err, facebookEvents) {
            if(facebookEvents.length > 0 && events.length > 0){
              events[events.length-1].lastRegularEvent = true;
            }
            res.json(events.concat(facebookEvents));
          })
        } else res.json(events);
      }
    });
  }



});

/* Get facebook events every 1 hour */
setInterval(function() {
  getFacebookEvents();
  console.log('----------------- FB -------------------')
}, 60000 * 60);

/* Get facebook events */
router.route('/getFacebookEvents').get(function (req, res) {
  getFacebookEvents();
});

function getFacebookEvents() {
  graph.get("me?fields=id,name,events.limit(1000){id,rsvp_status,place,cover,description,name,end_time,start_time,admins}", function(err, res) {
    let i, j, a, b;

    getAllFacebookEvents()
    .then(function(facebookEvents){
      getFacebookEventsBlackList()
      .then(function(blackList){
        getPersonalEvents()
        .then(function(personalEvents){

          for (i = 0; i < res.events.data.length; i++){
            // check black list
            if(blackList.length > 0){
              let isInBlackList = false;
              for (a = 0; a < blackList.length; a++){
                if(res.events.data[i].id == blackList[a].facebookId){
                  isInBlackList = true;
                  break;
                }
              }
              if(!isInBlackList){
                checkSimilarPersonalEvents();
              }
            } else {
              checkSimilarPersonalEvents();
            }
          }

          // check similar personal events
          function checkSimilarPersonalEvents(){
            if(personalEvents.length > 0){
              let isSimilar = false;
              for (b = 0; b < personalEvents.length; b++){
                let eventDate = new Date(personalEvents[b].destination.fromDate);
                let facebookDate = new Date(res.events.data[i].start_time);

                if(!res.events.data[i].place) {res.events.data[i].place = {}};

                if(personalEvents[b].description.indexOf(res.events.data[i].id) !== -1 ||
                   (personalEvents[b].contacts && personalEvents[b].contacts.indexOf(res.events.data[i].id) !== -1) ||
                   (eventDate.toDateString() == facebookDate.toDateString() && res.events.data[i].name && res.events.data[i].name.toLowerCase() == personalEvents[b].name.toLowerCase()) ||
                   (eventDate.toDateString() == facebookDate.toDateString() && res.events.data[i].place.location && res.events.data[i].place.location.city && res.events.data[i].place.location.city.toLowerCase() == personalEvents[b].destination.city.toLowerCase()) ||
                   (res.events.data[i].place.location && res.events.data[i].place.location.city && res.events.data[i].name && res.events.data[i].name.toLowerCase() == personalEvents[b].name.toLowerCase() && res.events.data[i].place.location.city.toLowerCase() == personalEvents[b].destination.city.toLowerCase())
                  ){
                  isSimilar = true;
                  break;
                } else {
                  // check links to facebook events
                  if(personalEvents[b].sitesList.length > 0){
                    for (let c = 0; c < personalEvents[b].sitesList.length; c++){
                      if(personalEvents[b].sitesList[c].indexOf('facebook') !== -1 &&
                        personalEvents[b].sitesList[c].indexOf('events') !== -1 &&
                        personalEvents[b].sitesList[c].indexOf(res.events.data[i].id) !== -1){
                        isSimilar = true;
                        break;
                      }
                    }
                  }

                }
              }
              if(!isSimilar){
                checkAddedFacebookEvents(res.events.data[i]);
              } else {
                // add to black list
                let fbEventId = res.events.data[i].id;
                let facebookEventBlackListEl = new FacebookEventBlackList({facebookId: fbEventId});
                facebookEventBlackListEl.save()
                .then(() => {
                  FacebookEvent.findByIdAndDelete(fbEventId, function (err) {
                    if(err){
                      console.log('Error: delete facebook event');
                    }
                    else {
                      console.log('Success: delete facebook event');
                    }
                  });
                })
              }
            } else checkAddedFacebookEvents(res.events.data[i]);
          }

          // check already added facebook events
          function checkAddedFacebookEvents(){
            if(res.events.data[i].cover){
              if(facebookEvents.length > 0){
                let isAlreadyAdded = false;
                let existedFacebookEventId;
                for (j = 0; j < facebookEvents.length; j++){
                  if(facebookEvents[j].facebookId == res.events.data[i].id){
                    isAlreadyAdded = true;
                    existedFacebookEventId = facebookEvents[j]._id;
                    break;
                  }
                }
                if(!isAlreadyAdded){
                  addFacebookEvent(res.events.data[i]);
                } else {
                  updateFacebookEvent(res.events.data[i], existedFacebookEventId);
                }
              } else addFacebookEvent(res.events.data[i]);
            }
          }

        })
      })
    })



    function addFacebookEvent(facebookEvent) {

      if(!facebookEvent.place) {facebookEvent.place = {}};

      let data = {
        image: facebookEvent.cover.source,
        name: facebookEvent.name,
        description: facebookEvent.description,
        isFacebookEvent: true,
        isOneTimeEvent: true,
        type: 'workshop',
        facebookId: facebookEvent.id,
        sitesList: ['https://www.facebook.com/events/' + facebookEvent.id],
        group: facebookEvent.admins ? facebookEvent.admins.data[0].name : "",
        destination: {
          latitude: facebookEvent.place.location ? facebookEvent.place.location.latitude : 0,
          longitude: facebookEvent.place.location ? facebookEvent.place.location.longitude : 0,
          countryLabel: facebookEvent.place.location ? facebookEvent.place.location.country : "",
          city: facebookEvent.place.location ? facebookEvent.place.location.city : "",
          address: facebookEvent.place.location ? facebookEvent.place.location.street : "",
          place: facebookEvent.place.name ? facebookEvent.place.name : "",
          fromDate: facebookEvent.start_time,
          toDate: facebookEvent.end_time,

        }
      }

      if(facebookEvent.place.location){
        for (let i = 0; i < countries.length; i++){
          if(countries[i].label == facebookEvent.place.location.country){
            data.destination.countryValue = countries[i].value;
            break;
          }
        }
      }


      let event = new FacebookEvent(data);

      event.save()
      .then(event => {
        console.log('Success: add events');
      //  mainRes.json(event);
      })
      .catch(err => {
        console.log('Error: add events');
        console.log(err);
       // mainRes.json(err);
      });
    }

    function updateFacebookEvent(facebookEvent, existedFacebookEventId) {

      if(!facebookEvent.place) {facebookEvent.place = {}};

      let data = {
        image: facebookEvent.cover.source,
        name: facebookEvent.name,
        description: facebookEvent.description,
        group: facebookEvent.admins ? facebookEvent.admins.data[0].name : "",
        destination: {
          latitude: facebookEvent.place.location ? facebookEvent.place.location.latitude : 0,
          longitude: facebookEvent.place.location ? facebookEvent.place.location.longitude : 0,
          countryLabel: facebookEvent.place.location ? facebookEvent.place.location.country : "",
          city: facebookEvent.place.location ? facebookEvent.place.location.city : "",
          address: facebookEvent.place.location ? facebookEvent.place.location.street : "",
          place: facebookEvent.place.name ? facebookEvent.place.name : "",
          fromDate: facebookEvent.start_time,
          toDate: facebookEvent.end_time,
        }
      }

      if(facebookEvent.place.location){
        for (let i = 0; i < countries.length; i++){
          if(countries[i].label == facebookEvent.place.location.country){
            data.destination.countryValue = countries[i].value;
            break;
          }
        }
      }

      FacebookEvent.findByIdAndUpdate(existedFacebookEventId, data, function (err, event) {
        if(err){
          console.log('Error: update FacebookEvent');
          console.log(err);
        }
        else {
          console.log('Success: update FacebookEvent');
        }
      });
    }

  });
}


function getAllFacebookEvents(){
  return FacebookEvent.find(function (err, facebookEvents) {
    return facebookEvents;
  })
}

function getFacebookEventsBlackList(){
  return FacebookEventBlackList.find(function (err, blackList) {
    return blackList;
  })
}


// Get workshops
function getPersonalEvents(){
  let parameters = `name
                    type
                    description
                    contacts
                    sitesList
                    destination.fromDate
                    destination.countryLabel
                    destination.city`;

  let options = { };

  let query = {
    type: 'workshop'
  };

  var today = new Date();
  var tomorrow = new Date();
  tomorrow.setDate(today.getDate()-1);

  query['destination.fromDate'] = {$gte: new Date(tomorrow)};
  options = {sort: {'destination.fromDate': 1}}
  return getPersonalEventsFromDb(query, parameters, options);
}

function getPersonalEventsFromDb(query, parameters, options){
  return Event.find(query, parameters, options, function (err, events) {
    if(!err){
      console.log('Success: got events');
      return events;
    }
  });
}


/* Get event by ID */
router.route('/:id').get(function(req, res) {
  Event.findById(req.params.id, function (err, event) {
    if(err){
      console.log('Error: got event');
      console.log(err);
    }
    else {
      console.log('Success: got event');
      console.log(event);
      res.json(event);
    }
  });

});

router.route('/getFacebookEvent/:id').get(function(req, res) {
  FacebookEvent.findById(req.params.id, function (err, event) {
    if(err){
      console.log('Error: got event');
      console.log(err);
    }
    else {
      console.log('Success: got event');
      console.log(event);
      res.json(event);
    }
  });

});

/* Delete event */
router.route('/delete/:id').get(function(req, res) {
  Event.findByIdAndDelete(req.params.id, function (err) {
    if(err){
      console.log('Error: delete event');
      console.log(err);
    }
    else {
      console.log('Success: delete event');
      res.status(200).send("Event deleted succesfully");
    }
  });

});

/* Update event */
router.route('/update/:id').post(function (req, res) {
  let event = new Event(req.body);
  Event.findByIdAndUpdate(req.params.id, event, function (err, event) {
    if(err || !event){
      console.log('Error: update event');
      console.log(err);
      res.status(500).send('Error: update event');
    }
    else {
      console.log('Success: update event');
      console.log(event);
      res.json(event);
    }
  });
});


/* Add event */
router.route('/add/:isConvert/:facebookEventId').post(function (req, res) {
  let event = new Event(req.body);
  let isConvert = req.params.isConvert;

  event.save()
    .then(event => {
      if(isConvert == 'true'){
        let facebookEventBlackListEl = new FacebookEventBlackList({facebookId: req.body.facebookId});
        facebookEventBlackListEl.save()
        .then(() => {
          FacebookEvent.findByIdAndDelete(req.params.facebookEventId, function (err) {
            if(err){
              console.log('Error: delete facebook event');
              console.log(err);
            }
            else {
              console.log('Success: delete facebook event');
              res.json(event);
            }
          });
        })
      } else res.json(event);

    })
    .catch(err => {
      console.log('Error: add events');
      res.status(400).send("unable to save to database");
    });
});

/* Upload image */
router.post('/uploadFile/:id', function (req, res, next) {
    var path = '';
    upload(req, res, function (err) {
      if (err) {
        // An error occurred when uploading
        return res.status(422).send("an Error occured")
      }
      // No error occured.
      path = req.file.path;
      cloudinary.uploader.upload(path, {
        folder: cloudinaryFolder,
        quality: "auto",
        fetch_format: "auto",
        transformation: [
          {width: "570", dpr: "auto", crop: "limit"}
        ]
      }, function(error, result) {
        if(result){
          if(result.url.indexOf("https") == -1){
            result.url = result.url.replace("http","https");
          }
          Event.findByIdAndUpdate(req.params.id, {image: result.url}, function (err, event) {
            if(err || !event){
              console.log('Error: update event');
              console.log(err);
              res.status(500).send('Error: update image');
            }
            else {
              console.log('Success: update event');
              console.log(event);
              return res.send("Upload Completed for "+path);
            }
          });
        }
      })
  });
})


module.exports = router;
