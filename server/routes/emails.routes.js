const express = require('express');
const nodeMailer = require('nodemailer');
const router = express.Router();


/* Send email */
router.route('/forgot-password').post(function (req, res) {

  let transporter = nodeMailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secureConnection: true,
    auth: {
        // should be replaced with real sender's account
        user: 'capoeira.map@gmail.com',
        pass: 'wF0eDJHufk'
    }
  });
  let mailOptions = {
      // should be replaced with real recipient's account
      to: req.body.email,
      subject: 'Reset password',
      html: '<p>Hello ' + req.body.name  + '!</p> <p>Here is your new password: <b>' + req.body.password + '</b></p> <p>Check it on <a href="https://www.capoeira-map.com">www.capoeira-map.com</a></p>'
  };
  transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          return console.log(error);
      }
      console.log('Message %s sent: %s to: %s', info.messageId, info.response, req.body.email);
  });
  res.end();
  
});

module.exports = router;