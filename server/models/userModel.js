// Users

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let userModel = new Schema({
  id: {type: Object},
  name: {type: String},
  surname: {type: String},
  apelido: {type: String},
  grad: {type: String},
  country: {
    label: String,
    value: String
  },
  city: {type: String},
  group: {type: String},
  gender: {type: String},
  email: {type: String},
  password: {type: String},
  age: {type: Number},
  image: {type: String},
  sitesList: {type: [String]},
  description: {type: String}
},
{
    collection: 'users'
});

module.exports = mongoose.model('userModel', userModel);