// Facebook Events black list

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let facebookEventBlackListModel = new Schema({
  facebookId: {type: Object}
},
{
    collection: 'facebookEventsBlackList'
});

module.exports = mongoose.model('facebookEventBlackListModel', facebookEventBlackListModel);