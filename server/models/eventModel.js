// Events

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let eventModel = new Schema({
  id: {type: Object},
  createdBy: {type: Object},
  name: {type: String},
  image: {type: String},
  type: {type: String},
  isFreeEvent: {type: Boolean},
  isOneTimeEvent: {type: Boolean},
  lastRegularEvent: {type: Boolean},
  destination: {
    countryLabel: {type: String},
    countryValue: {type: String},
    address: {type: String},
    city: {type: String},
    latitude: {type: Number},
    longitude: {type: Number},
    fromDate: {type: Date},
    toDate: {type: Date},
    fromTime: {type: Date},
    toTime: {type: Date},
    regularDays: {type: []},
    regularTimes: {type: []}
  },
  group: {type: String},
  description: {type: String},
  price: {type: String},
  guestsIdsList: {type: []},
  sitesList: {type: [String]},
  contacts: {type: String},
},
{
    collection: 'events'
});

module.exports = mongoose.model('eventModel', eventModel);