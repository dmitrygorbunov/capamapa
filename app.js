var createError = require('http-errors');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var compression = require('compression');
//var logger = require('morgan');

var mongoose = require('mongoose');

// Prod or test mongodb
if(process.env.PORT){
  // mLab
  // user: heroku_z2zxlmpp
  // password: wF0eDJHufk
  var bdURL = 'mongodb://heroku_z2zxlmpp:wF0eDJHufk@ds261526-a0.mlab.com:61526,ds261526-a1.mlab.com:61526/heroku_z2zxlmpp?replicaSet=rs-ds261526';
} else var bdURL = 'mongodb://localhost:27017/test';

mongoose.connect(bdURL, { promiseLibrary: require('bluebird') })
  .then(() =>  console.log('connection successful test'))
  .catch((err) => console.error(err));

var apiRouter = require('./server/routes/routes');
var eventsRoutes = require('./server/routes/events.routes');
var usersRoutes = require('./server/routes/users.routes');
var emailsRoutes = require('./server/routes/emails.routes');

var app = express();

app.use(compression());

// For CORS request
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();  
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'dist')));
app.use('/', express.static(path.join(__dirname, 'dist')));
app.use(favicon(__dirname + '/dist/favicon.ico'));

//app.use(express.static(path.join(__dirname, 'src')));
//app.use('/', express.static(path.join(__dirname, 'src')));

app.use('/api', apiRouter);
app.use('/events', eventsRoutes);
app.use('/users', usersRoutes);
app.use('/emails', emailsRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err.status);
});

module.exports = app;